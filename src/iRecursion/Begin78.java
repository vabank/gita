package iRecursion;

// Muallif: Aziz Ismoilov
// Sana: 26.01.2021
// Maqsad: maxElem() funksiyasini hosil qilish

import java.util.Scanner;

public class Begin78 {
    static int recursiveMax(int[] arr, int length)
    {
        if (length == 1)
            return arr[0];
        return max(recursiveMax(arr, length - 1), arr[length - 1]);
    }

    private static int max(int n1, int n2)
    {
        return Math.max(n1, n2);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Array nechta elementdan tashkil topsin oka: ");
        int a = scanner.nextInt();
        int[] arr = new int[a];
        for (int i = 0; i < a; i++) {
            System.out.println(i+1+" chi elementni kiriting brat");
            arr[i] = scanner.nextInt();
        }
        int max = recursiveMax(arr, arr.length);
        System.out.println("Maximum element: " + max);
    }
}
