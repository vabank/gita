package iRecursion;

// Muallif: Aziz Ismoilov
// Sana: 26.01.2021
// Maqsad: digitCount() funksiyasini hosil qilish

import java.util.Scanner;

public class Begin79 {
    public static int digitCounts(String str) {
        int counter = 0;
        if (str.length()>0) {
            if (Character.isDigit(str.charAt(0))) {
                counter++;
            }
            return counter + digitCounts(str.substring(1));
        }
        return counter;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        System.out.println(digitCounts(example));

    }
}
