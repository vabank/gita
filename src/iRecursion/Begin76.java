package iRecursion;
// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: EKUBni topish

import java.util.Scanner;

public class Begin76 {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        System.out.println("number1 = ");
        int number1 = scanner.nextInt();
        System.out.println("number1 = ");
        int number2 = scanner.nextInt();
        System.out.println("EKUB " + number1 +" va "
                + number2 +" sonlari uchun : " + findGCD(number1,number2));
    }

        private static int findGCD(int number1, int number2) {
            if(number2 == 0){
                return number1;
            }
            return findGCD(number2, number1%number2);
        }

}
