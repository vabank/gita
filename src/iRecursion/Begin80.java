package iRecursion;

// Muallif: Aziz Ismoilov
// Sana: 26.01.2021
// Maqsad: isPalindrome() funksiyasini hosil qilish

import java.util.Scanner;

public class Begin80 {
    public static boolean isPalindrome (String str) {

        if (str.length() < 2)
            return true;

        if (str.charAt (0) != str.charAt (str.length() - 1))
            return false;

        return isPalindrome (str.substring (1, str.length () - 1));
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        System.out.println(isPalindrome(example));
    }
}
