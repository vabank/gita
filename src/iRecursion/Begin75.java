package iRecursion;
// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: Pibonachi

import java.util.Scanner;

import static iRecursion.Begin74.fib;

public class Begin75 {

    public static void main(String args[]){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Fibonachi sonlar ketma-ketligidagi nechanchi sonni chiqazi oka: " +
                "3 ta xohlagan sonizi kirgazing: ");
        int[] number = new int[3];
        for (int i = 0; i < 3; i++) {
            System.out.println(i+1+" chi elemntni kirg'azing");
            number[i] = scanner.nextInt();
        }
        System.out.println();
        for (int i = 0; i < 3; i++) {
            System.out.print(fib2(number[i])+" ");
        }
        System.out.println(counter);

    }
    static int counter = 0;

    public static int fib2(int n) {
        counter++;
        if (n == 0 || n == 1) {
            return 1;
        }
        return fib(n - 1) + fib(n - 2);
    }

}
