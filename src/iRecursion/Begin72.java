package iRecursion;

// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: N sonigach bo'lgan raqamlar ko'paytmasini topuvchi(faktorial)

import java.util.Scanner;

public class Begin72 {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        System.out.println("(n>0) n sonini kiriting = ");
        int number1 = scanner.nextInt();
        System.out.println(findGCD(number1));
    }

        private static int findGCD(int number1) {
            if (number1 == 1) {
                return 1;
            }
            return number1*findGCD(number1-1);
        }

}
