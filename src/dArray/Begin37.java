package dArray;

// Muallif: Aziz Ismoilov
// Sana: 22.01.2021
// Maqsad: Array orasidan R soniga eng yaqin bo'lgan perviy element chiqarilsin

import java.util.Scanner;

public class Begin37 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int natija = 0, ayirma = Integer.MAX_VALUE;
        StringBuilder toplam = new StringBuilder();
        System.out.println("Oka Array nechta elementdan tashkil topsin: ");
        int a = scanner.nextInt();
        int[] myArray = new int[a];

        for (int i = 0; i < myArray.length; i++) {
            System.out.println((i+1)+" chi elementni kiriting: ");
            myArray[i] = scanner.nextInt();
            toplam.append(myArray[i]).append(" ");
        }
        System.out.println("R sonini kiriting");
        int R = scanner.nextInt();
        for (int j : myArray) {
            if (Math.abs(R - j) < ayirma) {
                ayirma = Math.abs(R - j);
                natija = j;
            }
        }
        System.out.println("To'plam: "+toplam+", Brat siz kutgan son: "+natija);

    }
}
