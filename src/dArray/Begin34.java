package dArray;

// Muallif: Aziz Ismoilov
// Sana: 22.01.2021
// Maqsad: Array bo'yicha

import java.util.Scanner;

public class Begin34 {

    public static void main(String[] args) {
        int elCount=0, elCount2=0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka Array nechta elementdan tashkil topsin: ");
        int a=scanner.nextInt();
        int[] myArray = new int[a];
        int[] secondArray = new int[a];

        for (int i = 0; i < myArray.length; i++) {
            System.out.println((i+1)+" chi elementni kiriting: ");
            myArray[i] = scanner.nextInt();
        }

        for (int s = 0; s < secondArray.length; s++) {
            if (s % 2 == 0) {
                elCount2++;
                secondArray[s] = myArray[elCount2-1];
            }else {
                elCount++;
                secondArray[s] = myArray[myArray.length-elCount];

            }
        }

        for (int j : secondArray) {
            System.out.print(j + " ");
        }
    }
}
