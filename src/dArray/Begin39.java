package dArray;

// Muallif: Aziz Ismoilov
// Sana: 22.01.2021
// Maqsad: m ta sondan n tasini takrorlanmaydigan qilib tanlab olish(shuffle)


import java.util.Random;
import java.util.Scanner;

public class Begin39 {

    static int[] getTest(int[] result){
        Random rand = new Random();
        for (int i = 0; i < result.length; i++) {
            int index = rand.nextInt(result.length);
            int temp = result[index];
            result[index] = result[i];
            result[i] = temp;
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka savollar sonini kiriting: ");
        int m = scanner.nextInt();
        System.out.println("Shu testdan nechta savol chiqaray: ");
        int n = scanner.nextInt();
        while (m<n){
            System.out.println("Savollar sonidan ortiq bo'lmasin. Shu testdan nechta savol chiqaray: ");
            n = scanner.nextInt();
        }
        int[] result = new int[n];

        for (int i = 0; i < n; i++) {
            result[i] = i + 1;
        }
        getTest(result);
        for (int i = 0; i < n; i++) {
            System.out.println(result[i]);
        }
    }
}
