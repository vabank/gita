package dArray;

// Muallif: Aziz Ismoilov
// Sana: 22.01.2021
// Maqsad: Endi lokal maksimumni chiqarish kerak

import java.util.Scanner;

public class Begin36 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka Array nechta elementdan tashkil topsin: ");
        int a=scanner.nextInt(), max=Integer.MAX_VALUE;
        int[] myArray = new int[a];

        for (int i = 0; i < myArray.length; i++) {
            System.out.println((i+1)+" chi elementni kiriting: ");
            myArray[i] = scanner.nextInt();
        }

        for (int i = 1; i < myArray.length - 1; i++) {
            if (myArray[i - 1] < myArray[i] && myArray[i] > myArray[i + 1]) {
                if (max > myArray[i]) {
                    max = myArray[i];
                }
                }
            }
        if (max == Integer.MAX_VALUE) {
            System.out.println("Bunday element jo'q");
        }else {
            System.out.println("Eng kichik lokal maksimum: " + max);
        }
    }
}
