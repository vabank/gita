package dArray;

// Muallif: Aziz Ismoilov
// Sana: 23.01.2021
// Maqsad: Tartibsiz sonlarni tartiblab chiqarish


import java.util.Scanner;

public class Begin42 {

    public static void insertionSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            int j = i - 1;
            while(j >= 0 && current < array[j]) {
                array[j+1] = array[j];
                j--;
            }
            array[j+1] = current;
        }
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String toplam = "";
        System.out.println("Oka Array nechta elementdan tashkil topsin: ");
        int a = scanner.nextInt();
        int[] myArray = new int[a];

        for (int i = 0; i < myArray.length; i++) {
            System.out.println((i+1)+" chi elementni kiriting: ");
            myArray[i] = scanner.nextInt();
            toplam = toplam + myArray[i]+" ";
        }

        insertionSort(myArray);

        System.out.println(toplam);
        for (int j : myArray) {
            System.out.print(j+" ");
        }
}
}
