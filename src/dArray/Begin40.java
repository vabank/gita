package dArray;

// Muallif: Aziz Ismoilov
// Sana: 22.01.2021
// Maqsad: Tartibsiz sonlarni tartiblab chiqarish


import java.util.Scanner;

public class Begin40 {

    public static void pufaksimonSaralash(int[] a) {
        boolean saralangan = false;
        int temp;
        while(!saralangan) {
            saralangan = true;
            for (int i = 0; i < a.length - 1; i++) {
                if (a[i] > a[i+1]) {
                    temp = a[i];
                    a[i] = a[i+1];
                    a[i+1] = temp;
                    saralangan = false;
                }
            }
        }
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String toplam = "";
        System.out.println("Oka Array nechta elementdan tashkil topsin: ");
        int a = scanner.nextInt();
        int[] myArray = new int[a];

        for (int i = 0; i < myArray.length; i++) {
            System.out.println((i+1)+" chi elementni kiriting: ");
            myArray[i] = scanner.nextInt();
            toplam = toplam + myArray[i]+" ";
        }

        pufaksimonSaralash(myArray);

        System.out.println(toplam);
        for (int j : myArray) {
            System.out.print(j+" ");
        }
}
}
