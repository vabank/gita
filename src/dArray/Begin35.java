package dArray;

// Muallif: Aziz Ismoilov
// Sana: 22.01.2021
// Maqsad: Array bo'yicha

import java.util.Scanner;

public class Begin35 {

    public static void main(String[] args) {
        StringBuilder toplam = new StringBuilder();
        int counter = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka Array nechta elementdan tashkil topsin: ");
        int a=scanner.nextInt();
        int[] myArray = new int[a];
        int[] secondArray = new int[a];


        for (int i = 0; i < myArray.length; i++) {
            System.out.println((i+1)+" chi elementni kiriting: ");
            myArray[i] = scanner.nextInt();
            toplam.append(myArray[i]).append(" ");
        }

        for (int i = 0; i < secondArray.length; i++) {
            if (counter % 2 == 0) {
                secondArray[i+counter] = myArray[counter];
                secondArray[i+counter + 1] = myArray[counter + 1];
            } else {
                secondArray[i+counter] = myArray[a-counter];
                secondArray[i+counter+1] = myArray[a-(counter+1)];
            }
            counter++;
            if (counter == secondArray.length / 2) {
                if (secondArray.length % 2 == 1) {
                    secondArray[secondArray.length - 1] = myArray[secondArray.length / 2 + 1];
                }
                break;

            }
        }

        System.out.println(toplam);
        for (int j : secondArray) {
            System.out.print(j + " ");
        }
    }
}
