package dArray;

// Muallif: Aziz Ismoilov
// Sana: 22.01.2021
// Maqsad: Arrayda qatnashgan son nechtadan qatnashganini aniqlovchi dastur tuzish


import java.util.*;

public class Begin38 {

    public static int[] mySort(int[] myArray) {
        for (int i = 0; i < myArray.length; i++) {
            for (int j = i + 1; j < myArray.length; j++) {
                int tmp = 0;
                if (myArray[i] > myArray[j]) {
                    tmp = myArray[i];
                    myArray[i] = myArray[j];
                    myArray[j] = tmp;
                }
            }
        }
        return myArray;
    }

    private static void count(int[] arr) {

        mySort(arr);

        int sum = 0, counter = 0;

        for (int i = 0; i < arr.length; i++) {

            if (arr[0] == arr[arr.length - 1]) {
                System.out.println(arr[0] + ": " + counter + " times");

                break;
            } else {
                if (i == (arr.length - 1)) {
                    sum += arr[arr.length - 1];
                    counter++;
                    System.out.println((sum / counter) + " : " + counter
                            + " times");

                    break;
                } else {
                    if (arr[i] == arr[i + 1]) {
                        sum += arr[i];
                        counter++;
                    }
                    else if (arr[i] != arr[i + 1]) {
                        sum += arr[i];
                        counter++;
                        System.out.println((sum / counter) + " : " + counter
                                + " times");
                        sum = 0;
                        counter = 0;
                    }
                }
            }
        }

    }


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int  lop=0,ayirma = Integer.MAX_VALUE;
        HashMap<Integer, Integer> map = new HashMap<>();
        String toplam = "",toplam2 = "";
        System.out.println("Oka Array nechta elementdan tashkil topsin: ");
        int a = scanner.nextInt();
        int[] myArray = new int[a];

        for (int i = 0; i < myArray.length; i++) {
            System.out.println((i+1)+" chi elementni kiriting: ");
            myArray[i] = scanner.nextInt();
            toplam = toplam + myArray[i]+" ";
        }

        System.out.println("To'plam: "+toplam+" Siz kutgan javob: ");
        count(myArray);

    }
}
