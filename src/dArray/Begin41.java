package dArray;

// Muallif: Aziz Ismoilov
// Sana: 23.01.2021
// Maqsad: Array selection sort

import java.util.HashMap;
import java.util.Scanner;

public class Begin41 {

    public static int[] selectionSort(int[] myArray) {
        for (int i = 0; i < myArray.length; i++) {
            for (int j = i + 1; j < myArray.length; j++) {
                int tmp = 0;
                if (myArray[i] > myArray[j]) {
                    tmp = myArray[i];
                    myArray[i] = myArray[j];
                    myArray[j] = tmp;
                }
            }
        }
        return myArray;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int  lop=0,ayirma = Integer.MAX_VALUE;
        HashMap<Integer, Integer> map = new HashMap<>();
        String toplam = "",toplam2 = "";
        System.out.println("Oka Array nechta elementdan tashkil topsin: ");
        int a = scanner.nextInt();
        int[] myArray = new int[a];

        for (int i = 0; i < myArray.length; i++) {
            System.out.println((i+1)+" chi elementni kiriting: ");
            myArray[i] = scanner.nextInt();
            toplam = toplam + myArray[i]+" ";
        }

        selectionSort(myArray);

        System.out.println(toplam);
        for (int j : myArray) {
            System.out.print(j+" ");
        }
}
}
