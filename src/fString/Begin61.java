package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Birinchi katalog nomni chiqaruvchi dastur

import java.util.Scanner;

public class Begin61 {

    static String getCatalog(String s) {
        int endSlash=0, counter = 0, counter2=0;
        String result = "";
        for(int i = 0; i <s.length(); i++){
            counter++;
            if (s.charAt(i) == 92) {
                counter2++;
                if (counter2 == 2) {
                    for (int j = endSlash; j < counter - 1; j++) {
                        result = result + s.charAt(j);
                    }
                } else {
                    result = "/";
                }
                endSlash = counter;
            }

        }
        if (counter2 == 0) {
            char w = 92;
            return " " + w;
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        example = example + " ";
        System.out.println(getCatalog(example));
    }
}

