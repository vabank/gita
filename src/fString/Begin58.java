package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Satrdagi barcha probellarni olib tashlash

import java.util.Scanner;

public class Begin58 {

    static String removeSpace(String s) {
        int min = Integer.MAX_VALUE, length = 0, start=0, end=0, counter = 0;
        String result = "", originalResult="";

        for(int i = 0; i <s.length(); i++){

            if (s.charAt(i) != ' ') {
                if (result.equals("")) {
                    result = result + s.charAt(i);
//                    result = result.toUpperCase();
                } else {
                    result = ""+s.charAt(i);
                }
                    originalResult = originalResult + result;
                counter = 0;
            }else {
                result = "";
                counter++;
                if (counter == 1) {
                    originalResult = originalResult + " ";
                }
            }
        }
        return originalResult;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        example = example + " ";
        System.out.println(removeSpace(example));
    }
}
