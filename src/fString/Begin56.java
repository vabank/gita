package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Satrdagi eng qisqa so'zni chiqarish

import java.util.Scanner;

public class Begin56 {

    static String engQisqaSoz(String s) {
        int min = Integer.MAX_VALUE, length = 0, start=0, end=0, counter = 0;
        String result = "", originalResult="";

        for(int i = 0; i <s.length(); i++){

            if (s.charAt(i) != ' ') {
                result = result + s.charAt(i);
                length = result.length();
            }else {
            if (length < min) {
                min = length;
                originalResult = result;

            }
                result = "";
            }
        }
        return "Enh qisqa so'z: "+originalResult+"Uzunligi: "+min;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        example = example + " ";
        System.out.println(engQisqaSoz(example));
    }
}
