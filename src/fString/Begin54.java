package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Satrdagi 1- va oxirgi probel o'rtasidagi belgilarni chiqarish

import java.util.Scanner;

public class Begin54 {

    static String countNumChars(String s) {
        int space = 0, start=0, end=0, counter = 0;
        String result = "";

        for(char c : s.toCharArray()){
            counter++;
            if (c==' '){
                space++;
                if (space == 1) {
                    start = counter;
                }
                end = counter;
            }
        }

        if (space == 1) {
            return "Bunday satr yo'q";
        } else {
            for (int i = start; i < end; i++) {
                result = result + s.charAt(i);
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        System.out.println(countNumChars(example));
    }
}
