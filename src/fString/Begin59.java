package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Fayl nomini ajratib olish

import java.util.Scanner;

public class Begin59 {

    static String getFileName(String s) {
        int endSlash=0, counter = 0;
        String result = "";
        for(int i = 0; i <s.length(); i++){
            counter++;
            if (s.charAt(i) == 92) {
                endSlash = counter;
            } else if (s.charAt(i)=='.') {
                for (int j = endSlash; j < counter-1; j++) {
                    result = result + s.charAt(j);
                }
            }

        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        example = example + " ";
        System.out.println(getFileName(example));
    }
}

