package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Qonuniyatni buzgan birinchi belgini chiqazish

import java.util.Scanner;

public class Begin63 {

    static String detectError(String example) {
        int a = 0, prev = 0, counter = 0;
        boolean isSequence = false;
        String result = "";
        for(int i=0; i < example.length(); i++) {
            boolean flag = Character.isDigit(example.charAt(i));
            if(flag) {
                System.out.println("'"+ example.charAt(i)+"' is a number");
            } else {
                counter++;
                System.out.println("'"+ example.charAt(i)+"' is a letter");
                a = example.charAt(i);

                if (counter != 1) {
                    isSequence = (a - prev == 1) ? true : false;
                    if (!isSequence) {
                        result = "" + example.charAt(i);
                        break;
                    } else {
                        result= "0";
                    }

                }
                prev = a;
            }
        }
        return "Siz kutvotgan natija okam: "+result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        example = example + "";
        System.out.println(detectError(example));

    }
}

