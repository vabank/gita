package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Satrdagi so'zlar sonini aniqlash


import java.util.Scanner;

public class Begin55 {
    static int countNumChars(String s) {
        int counter = 0;
        for(char c : s.toCharArray()){
            if (c == ' ') {
                counter++;
            }
        }
        return counter;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String example = scanner.nextLine();
        System.out.println(countNumChars(example)+1);
    }
}
