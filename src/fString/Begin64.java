package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Qavslar konkretniy qo'yilganini aniqlash

import java.util.Scanner;

public class Begin64 {

    static String detectError(String example) {
        int a = 0, counterOpen = 0, counterClose = 0;
        boolean isSequence = false;
        String result = "";
        for(int i=0; i < example.length(); i++) {
            a = example.charAt(i);
            if (a == 40) {
                counterOpen++;
            } else if (a == 41) {
                counterClose++;
                if (counterOpen > counterClose) {
                    result = "-1";
                } else if (counterOpen < counterClose) {
                    result = "" + i;
                } else {
                    result="0";
                }
            }
        }
        return "Siz kutvotgan natija okam: "+result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        example = example + " ";
        System.out.println(detectError(example));
    }
}

