package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Fayl kengaytmasini ajratib oling

import java.util.Scanner;

public class Begin60 {

    static String getFileExtension(String s) {
        int counter = 0;
        String result = "";
        for(int i = 0; i <s.length(); i++){
            counter++;
            if (s.charAt(i) == '.') {
                for (int j = counter; j <s.length() ; j++) {
                    result = result + s.charAt(j);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        example = example + " ";
        System.out.println(getFileExtension(example));
    }
}
