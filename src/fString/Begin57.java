package fString;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: Barcha so'zlarning 1-harfini katta qilish

import java.util.Scanner;

public class Begin57 {

    static String engQisqaSoz(String s) {
        int min = Integer.MAX_VALUE, length = 0, start=0, end=0, counter = 0;
        String result = "", originalResult="";

        for(int i = 0; i <s.length(); i++){

            if (s.charAt(i) != ' ') {
                if (result.equals("")) {
                    result = result + s.charAt(i);
                    result = result.toUpperCase();
                } else {
                    result = ""+s.charAt(i);
                }
                    originalResult = originalResult + result;

            }else {
                result = "";
                originalResult = originalResult +" ";
            }
        }
        return originalResult;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka u bu narsa yozvoring:");
        String example = scanner.nextLine();
        example = example + " ";
        System.out.println(engQisqaSoz(example));
    }
}
