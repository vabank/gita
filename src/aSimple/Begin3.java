package aSimple;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: Kiritilgan ikkita haqiqiy sonnning kichigini sonlarning yig'indisining yarmiga,
// ikkinchisini esa ko'paytmasining ikkilanganiga almashtirish

public class Begin3 {

    public static void enter(){
        Scanner scanner = new Scanner(System.in);
        double a, b;
        System.out.print("a = ");
        a = scanner.nextDouble();

        System.out.print("b = ");
        b = scanner.nextDouble();
        reverse(a, b);
    }

    public static void reverse(double a, double b) {
        if (a != b) {
            if (a > b) {
                System.out.println("a = " + 2 * a * b + ", b = " + (a + b)/2);
            } else {
                System.out.println("a = " + (a + b)/2 + ", b = " + 2 * a * b);
            }
        }else {
            System.out.println("a = " + a + ", b = " + b);
        }
    }

    public static void main(String[] args) {
        enter();
    }
}
