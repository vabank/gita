package aSimple;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: N sonigacha bo'lgan tub sonlarni chiqarish

public class Begin6 {

    public static void getPrimes(int n) {

        String  primeNumbers = "";
        int i =0;
        int num =0;

        for (i = 1; i <= n; i++) {
            int counter=0;
            for(num =i; num>=1; num--) {
                if(i%num==0) {
                    counter = counter + 1;
                }
            }
            if (counter ==2) {
                primeNumbers = primeNumbers + i + " ";
            }
        }
        System.out.println("Prime numbers from 1 to n are :");
        System.out.println(primeNumbers);
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the value of n:");
        int n = scanner.nextInt();
        scanner.close();
        getPrimes(n);

    }
}
