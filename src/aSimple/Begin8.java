package aSimple;
// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: Kredit nechi oydan so'ng ikki baravaridan ko'p bo'lishini topish

import java.util.Scanner;

public class Begin8 {

    static String detectCreditMonth(double summa, int percent){
        int month = 0;
        double lastSum = 0f,  oshganSumma = 0f;

        while (lastSum<2*summa){
            lastSum = (summa+oshganSumma) * (1 + percent/100f);
            oshganSumma = lastSum - summa;
            month += 1;
        }

        return "Muddat: "+month+", Summa: "+lastSum;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka summani kirg'azing: ");
        int summa = scanner.nextInt();
        System.out.println("Oka foizni kirg'azing: ");
        int percent = scanner.nextInt();

        System.out.println(detectCreditMonth(summa, percent));
    }

}
