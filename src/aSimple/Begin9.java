package aSimple;
// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: Raqamlar soni va raqamlar yig'indisi

import java.util.Scanner;

public class Begin9 {

    static String detectCreditMonth(int n){
        int counter = 1, sum = 0;
        while (n >=10) {
            counter++;
            sum = sum + n % 10;
            n = n / 10;

        }
        sum = sum + n;

        return "Raqamlar soni: "+counter+", Summa: "+sum;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka son kirg'azing: ");
        int n = scanner.nextInt();
        System.out.println(detectCreditMonth(n));
    }
}
