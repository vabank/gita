package aSimple;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: Kiritilgan sanadan keyingi kunni sanani chiqarish

public class Begin4 {
    public static String getNextDate(int year, int month, int day) throws ParseException {
        String curDate = year + "/" + month + "/" + day;
        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        final Date date = format.parse(curDate);
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return format.format(calendar.getTime());
    }

    public static void enterDate() throws ParseException {
        Scanner scanner = new Scanner(System.in);
        int year, month, day;
        System.out.println("Enter year: ");
        year = scanner.nextInt();
        int leapYear=Begin2.isLeapYear(year);
        if (leapYear == 366) {
            System.out.println("Kabisa bo'lmagan yil kiriting");
            enterDate();
        }else {
            System.out.println("Enter month: ");
            month = scanner.nextInt();
            if (month > 12) {
                System.out.println("Bunday oy yo'q Ahmoq!!");
                enterDate();
            }
            System.out.println("Enter day: ");
            day = scanner.nextInt();
            switch (month){
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    if (day>31){
                        System.out.println("Kunni noto'g'ri kiriting ahmoq!!!");
                    enterDate();} break;
                case 2: if (day>28){enterDate();} break;
                case 4:
                case 6:
                case 9:
                case 11:
                    if (day>30){
                        System.out.println("Kunni noto'g'ri kiriting ahmoq!!!");
                        enterDate();} break;
            }

            System.out.println(getNextDate(year, month, day));
        }

    }

    public static void main(String[] args) throws ParseException {
        enterDate();
    }
}
