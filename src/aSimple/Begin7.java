package aSimple;
// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: N sonigacha amicable(do'st) sonlarni chiqarish

import java.util.Scanner;

public class Begin7 {

    static int findSumOfDivisors(int num) {
        int sum = 1, div, i;

        for (i = 2; i <= (div = num / i); i++) {
            if (num % i == 0) {
                sum += i;
                if (i == div)
                    break;
                sum += div;
            }
        }
        return sum;
    }

    static  void amicableNumbers(int n) {
        int sum, i;

        for (i = 2; i <= n; i++) {
            sum = findSumOfDivisors(i);
            if (sum > i && sum <= n && findSumOfDivisors(sum) == i)
                System.out.printf("%d and %d\n", i, sum);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka son kiriting: ");
        int a = scanner.nextInt();
        amicableNumbers(a);
        System.out.println(findSumOfDivisors(284));
    }
}

