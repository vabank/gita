package aSimple;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: Kiritilgan yilda nechta kun borligini aniqlash

public class Begin2 {

    public static int isLeapYear(int year) {

            if (year % 4 != 0) {
                return 365;
            } else if (year % 400 == 0) {
                return 366;
            } else if (year % 100 == 0) {
                return 365;
            } else {
                return 366;
            }
        }

    public static  void isInclusive(int year){
            if (year <= 0) {
               enterYear();
            }else {
                System.out.println(isLeapYear(year));
            }
        }

    public static void enterYear(){
            Scanner scanner = new Scanner(System.in);
            System.out.println("Yil kiriting: ");
            int a = scanner.nextInt();
            isInclusive(a);
        }

    public static void main(String[] args) {
        enterYear();
    }
}
