package aSimple;
// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: N sonini M soniga bo'lganda qoldiqni va butun sonni topish(/ va % ni ishlatmagan holda)

import java.util.Scanner;

public class Begin11 {

    public static String bVaQoldiq(int a, int b) {
        int qoldiq = 0, butun = 0;

        if (a < b) {
            qoldiq = a;
        }
        while (a >=b) {
            a -= b;
            butun += 1;
            if (a < b) {
                qoldiq = a;
            }
        }
        return "Qoldiq:" + qoldiq + ", " + "Butun: " + butun;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("n=");
        int n = scanner.nextInt();
        System.out.println("m=");
        int m = scanner.nextInt();
        System.out.println(bVaQoldiq(n,m));
    }
}
