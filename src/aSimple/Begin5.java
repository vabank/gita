package aSimple;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: Mukammal sonlarni chiqarish

public class Begin5 {

    static boolean isPerfect(int n) {
        if (n == 1)
            return false;

        int sum = 1;

        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                sum += i;
            }
        }
        return sum == n;
    }

    public static void getResult(int a){

        List<Integer> perfectNumber = new ArrayList<>();
        for (int i = 0; i <=a; i++) {
            boolean isPer=isPerfect(i);
            if (isPer){
                perfectNumber.add(i);
            }
        }
        System.out.println(perfectNumber);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number: ");
        int a = scanner.nextInt();
        getResult(a);
    }
}
