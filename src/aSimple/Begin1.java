package aSimple;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 19.01.2021
// Maqsad: Sonlarning kichigini aniqlash

public class Begin1 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int a, b, c;
        System.out.println("a = ");
        a = scanner.nextInt();
        System.out.println("b = ");
        b = scanner.nextInt();
        System.out.println("c = ");
        c = scanner.nextInt();


        if (a <= b && a < c)
            System.out.println( a + " is the smallest");

        else if (b < a && b < c)
            System.out.println( b + " is the smallest");

        else
            System.out.println( c + " is the smallest");

    }

}

