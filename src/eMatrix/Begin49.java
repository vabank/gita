package eMatrix;

// Muallif: Aziz Ismoilov
// Sana: 24.01.2021
// Maqsad: mxn o'lchamli matritsia berilgan. Har bir satrdan eng kattasini
// n- ustunga biriktiruvchi dastur tuzilsin


import java.util.Scanner;

public class Begin49 {

    public static void getResult(int m, int n) {
        int max = Integer.MIN_VALUE;
        int[][] result = new int[m][n];
        int[][] realResult = new int[m][n+1];
        Scanner scanner = new Scanner(System.in);
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                    System.out.println("result["+(i-1)+"]["+(j-1)+"] ning qiymatini kiriting: ");
                    int a = scanner.nextInt();
                    result[i - 1][j - 1] = a;
                }
            }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j <=n; j++) {

                if (j == n) {
                    realResult[i][j] = max;
                } else {
                    if (result[i][j] > max) {
                        max = result[i][j];
                    }
                    realResult[i][j] = result[i][j];
                }
                System.out.print(realResult[i][j]);
            }
            System.out.println();
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j <=n; j++) {
                System.out.print(realResult[i][j]+" ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qatorlar sonini kiriting m = ");
        int m = scanner.nextInt();
        System.out.println("Ustunlar sonini kiriting n = ");
        int n = scanner.nextInt();
        getResult(m, n);
    }
}
