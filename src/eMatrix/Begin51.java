package eMatrix;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: mxn o'lchamli matritsaning eng kichik elementining
// indeksini chiqaruvchi dastur tuzilsin


import java.util.Scanner;

public class Begin51 {

    public static void getResult(int m, int n) {
        Scanner scanner = new Scanner(System.in);
        int min = Integer.MAX_VALUE;
        String originalResult = "";
        int[][] result = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.println("result["+i+"]["+j+"] chi elementni kiriting: ");
                result[i][j] = scanner.nextInt();
                if (result[i][j] < min) {
                    min = result[i][j];
                    originalResult = "Eng kichik son: "+min + "/Index: " + i + " " + j;
                }
            }
        }
        System.out.println(originalResult);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qatorlar sonini kiriting m = ");
        int m = scanner.nextInt();
        System.out.println("Ustunlar sonini kiriting n = ");
        int n = scanner.nextInt();
        getResult(m, n);
    }
}
