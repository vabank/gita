package eMatrix;

// Muallif: Aziz Ismoilov
// Sana: 24.01.2021
// Maqsad: mxn o'lchamli matrotsaning elementlarini spiral ko'riishda chiqarish


import java.util.Scanner;

public class Begin44 {

    public static void getResult(int m, int n) {
        int x = 1;
        for (int i = 1; i <= m; i++) {

            for (int j = 1; j <= n; j++) {
                if (i % 2 == 1) {
                    System.out.printf("%3d", x++);
                } else {
                    System.out.printf("%3d",((n*2*i)-(n-1)-x++));
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qatorlar sonini kiriting m = ");
        int m = scanner.nextInt();
        System.out.println("Ustunlar sonini kiriting n = ");
        int n = scanner.nextInt();
        getResult(m, n);
    }
}
