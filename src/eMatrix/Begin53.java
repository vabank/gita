package eMatrix;

// Muallif: Aziz Ismoilov
// Sana: 25.01.2021
// Maqsad: mxn o'lchamli matritsaning eng kichik element turgan
// satrni olib tashlovchi dastur tuzish


import java.util.Scanner;

public class Begin53 {

    public static void getResult(int m, int n) {
        Scanner scanner = new Scanner(System.in);
        int min = Integer.MAX_VALUE, columnIndex = 0;
        int[][] result = new int[m][n];
        int[][] originalResult = new int[m-1][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.println("result["+i+"]["+j+"] chi elementni kiriting: ");
                result[i][j] = scanner.nextInt();
                if (result[i][j] < min) {
                    min = result[i][j];
                    columnIndex = i;
                }
            }
        }
        
        System.out.println("Oka siz kutgan javob: ");
        for (int i = 0; i <m; i++) {
            if (i != columnIndex) {
                for (int j = 0; j < 1; j++) {
                    for (int k = 0; k < n; k++) {
                        originalResult[j][k] = result[i][k];
                        System.out.print(originalResult[j][k]+" ");
                    }
                    System.out.println();
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qatorlar sonini kiriting m = ");
        int m = scanner.nextInt();
        System.out.println("Ustunlar sonini kiriting n = ");
        int n = scanner.nextInt();
        getResult(m, n);
    }
}
