package eMatrix;

// Muallif: Aziz Ismoilov
// Sana: 24.01.2021
// Maqsad: mxn o'lchamli matrotsaning elementlarini spiral ko'riishda chiqarish


import java.util.Scanner;

public class Begin45 {

    public static void getResult(int m) {
        int x = 1, y, counter = 1, counter2=0;
        int n;

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= 2*(m-i)+1; j++) {
                y = m;
                if (j >(m - counter2)) {
                    n = x+y*counter-1;
                    System.out.printf("%3d", n);
                    counter++;
                } else {
                    System.out.printf("%3d", x++);
                }
            }

            counter2++;
            x = counter2 * m + 1;
            counter = 1;
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("mxm olchamli matritsa uchun m ning qiymatini kiriting m = ");
        int m = scanner.nextInt();
        int x = 1;

        for (int i = 1; i <= m; i++) {

            for (int j = 1; j <= m; j++) {
                System.out.printf("%3d", x++);
            }
            System.out.println();
        }
        System.out.println("Javob: ");
        getResult(m);
    }
}
