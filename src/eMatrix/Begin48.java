package eMatrix;

// Muallif: Aziz Ismoilov
// Sana: 24.01.2021
// Maqsad: mxn o'lchamli matritsianing satrida manfiy va busbat
// sonlar teng bo'lgan dastlabki satr tartib raqamini chiqarish


import java.util.Scanner;

public class Begin48 {

    public static void getResult(int m, int n) {
        int minus = 0;
        int plus= 0;
        int[][] result = new int[m][n];
        Scanner scanner = new Scanner(System.in);
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                System.out.println("result["+(i-1)+"]["+(j-1)+"] ning qiymatini kiriting: ");
                result[i - 1][j - 1] = scanner.nextInt();
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(result[i][j]+"   ");
            }
            System.out.println();
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (result[i][j] > 0) {
                    plus++;
                } else if (result[i][j]<0) {
                    minus++;
                }
            }
            if (plus == minus) {
                System.out.println("Javob: "+i);
                break;
            } else if (i == m - 1) {
                System.out.println("Bunday satr jo'q");
            } else {
                plus = 0;
                minus = 0;
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qatorlar sonini kiriting m = ");
        int m = scanner.nextInt();
        System.out.println("Ustunlar sonini kiriting n = ");
        int n = scanner.nextInt();
        getResult(m, n);
    }
}
