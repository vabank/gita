package eMatrix;

// Muallif: Aziz Ismoilov
// Sana: 24.01.2021
// Maqsad: mxn o'lchamli matrotsaning elementlarini elementlari ko'paytmasi


import java.util.Scanner;

public class Begin47 {

    public static void getResult(int m, int n) {
        int x = 1;
        int sum = 1;
        int[][] result = new int[m][n];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                result[i - 1][j - 1] = x++;
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(result[i][j]+"   ");
            }
            System.out.println();
        };

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                sum = sum * result[j][i];
            }
            System.out.print(sum+"  ");
            sum = 1;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qatorlar sonini kiriting m = ");
        int m = scanner.nextInt();
        System.out.println("Ustunlar sonini kiriting n = ");
        int n = scanner.nextInt();
        getResult(m, n);
    }
}
