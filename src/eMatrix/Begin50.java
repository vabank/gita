package eMatrix;

// Muallif: Aziz Ismoilov
// Sana: 24.01.2021
// Maqsad: mxn o'lchamli matritsa ustuninig
// eng katta sonini m-satrga joylashtirish


import java.util.Scanner;

public class Begin50 {

    public static void getResult(int m, int n) {
        Scanner scanner = new Scanner(System.in);
        int max;
        int[][] result = new int[m][n];
        int[][] originalResult = new int[m+1][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.println("result["+i+"]["+j+"] chi elementni kiriting: ");
                result[i][j] = scanner.nextInt();
                System.out.print(result[i][j] + "   ");
            }
            System.out.println();
        }

        System.out.println("Oka siz kutgan javob: ");
        for (int i = 0; i <=m; i++) {
            for (int j = 0; j < n; j++) {
                max = Integer.MIN_VALUE;
                if (i== m) {
                    for (int k = 0; k < m; k++) {
                        if (result[k][j] > max) {
                            max = result[k][j];
                        }
                    }
                    originalResult[i][j] = max;
                } else {
                    originalResult[i][j] = result[i][j];
                }
                System.out.print(originalResult[i][j]+"   ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qatorlar sonini kiriting m = ");
        int m = scanner.nextInt();
        System.out.println("Ustunlar sonini kiriting n = ");
        int n = scanner.nextInt();
        getResult(m, n);
    }
}
