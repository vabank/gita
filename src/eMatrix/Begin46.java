package eMatrix;

// Muallif: Aziz Ismoilov
// Sana: 24.01.2021
// Maqsad: mxn o'lchamli matrotsaning elementlarini elementlari summasi


import java.util.Scanner;

public class Begin46 {

    public static void getResult(int m, int n) {
        int x = 1;
        int sum;
        for (int i = 1; i <= m; i++) {
            sum = 0;
            for (int j = 1; j <= n; j++) {
                    sum = sum + x;
                    System.out.printf("%3d", x++);

            }
            System.out.println(" Summa => "+sum);
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qatorlar sonini kiriting m = ");
        int m = scanner.nextInt();
        System.out.println("Ustunlar sonini kiriting n = ");
        int n = scanner.nextInt();
        getResult(m, n);
    }
}
