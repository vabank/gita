package bMinMax;
// Muallif: Aziz Ismoilov
// Sana: 20.01.2021
// Maqsad: Kiritilgan to'plamdagi yig'indis eng katta juftlikni toping

import java.util.Scanner;

public class Begin21 {

    static void getMinMax() {
        int min1=0, min2=0, yigindi = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta sonli to'plam yaratmoqchisiz: ");
        int a = scanner.nextInt();
        StringBuilder toplam = new StringBuilder();

        for (int i = 1; i < a+1; i++) {

            System.out.println("Davay "+i+" chi sonni kiriting to'plamga: ");
            int b=scanner.nextInt();

            if (i % 2 != 0) {
                min1 = b;
            }else {
                min2 = b;
            }

            yigindi = (i > 1 && (min1 + min2) > yigindi) ? min1 + min2 : 0;

            toplam.append(b).append(" ");
        }
        System.out.println("To'plam: "+toplam+"Yig'indi: " + yigindi);
    }

    public static void main(String[] args) {
        getMinMax();
    }
}
