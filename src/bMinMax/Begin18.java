package bMinMax;
// Muallif: Aziz Ismoilov
// Sana: 20.01.2021
// Maqsad: Kiritilgan to'plamdagi birinchi uchragan eng katta toq son va uning pozitsiyasi

import java.util.Scanner;

public class Begin18 {

    static void getMinMax() {

        int max = 0, min = Integer.MAX_VALUE, position = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta sonli to'plam yaratmoqchisiz: ");
        int a = scanner.nextInt();
        StringBuilder toplam = new StringBuilder();

        for (int i = 0; i < a; i++) {

            System.out.println("Davay "+(i+1)+"chi sonni kiriting to'plamga: ");
            int b=scanner.nextInt();

            if (max <=b) {
                if (b % 2 != 0) {
                    max = b;
                    position = i + 1;
                }
            }
            toplam.append(b).append(" ");
        }
        System.out.println("Toq son: "+max+", Pozitsiya: "+position);


    }

    public static void main(String[] args) {
        getMinMax();
    }
}
