package bMinMax;
// Muallif: Aziz Ismoilov
// Sana: 20.01.2021
// Maqsad: Kiritilgan to'plamdagi eng kichik va eng katta sonni topish

import java.util.Scanner;

public class Begin12 {

    static void getMinMax() {
        int max=0, min=Integer.MAX_VALUE, myNum = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta sonli to'plam yaratmoqchisiz: ");
        int a = scanner.nextInt();
        StringBuilder toplam = new StringBuilder();
        for (int i = 0; i < a; i++) {

            System.out.println("Davay "+(i+1)+"chi sonni kiriting to'plamga: ");
            int b=scanner.nextInt();


            if (max < b) {
                max = b;
            }

            if (min > b) {
                min = b;
            }

            toplam.append(b).append(" ");
        }
        System.out.println("Umumiy to'plam: "+toplam+"\nMin: "+min+", Max: "+max);
    }

    public static void main(String[] args) {
        getMinMax();
    }

}
