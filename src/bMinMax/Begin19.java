package bMinMax;
// Muallif: Aziz Ismoilov
// Sana: 20.01.2021
// Maqsad: Kiritilgan to'plamdagi eng katta elementni va undan keyin nechta son borligini aniqlash

import java.util.Scanner;

public class Begin19 {

    static void getMinMax() {

        int max = 0, min = Integer.MAX_VALUE, position = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta sonli to'plam yaratmoqchisiz: ");
        int a = scanner.nextInt();
        StringBuilder toplam = new StringBuilder();

        for (int i = 0; i < a; i++) {

            System.out.println("Davay "+(i+1)+"chi sonni kiriting to'plamga: ");
            int b=scanner.nextInt();

            if (max <=b) {
                    max = b;
                position = a - i-1;

            }
            toplam.append(b).append(" ");
        }
        System.out.println("Son: "+max+", Nechta son bor: "+position);


    }

    public static void main(String[] args) {
        getMinMax();
    }
}
