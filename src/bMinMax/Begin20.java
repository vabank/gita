package bMinMax;
// Muallif: Aziz Ismoilov
// Sana: 20.01.2021
// Maqsad: Kiritilgan to'plamdagi eng kichik 2 taa elementni aniqlash

import java.util.Scanner;

public class Begin20 {

    static void getMinMax() {
        int min1=Integer.MAX_VALUE, min2=Integer.MAX_VALUE-1;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta sonli to'plam yaratmoqchisiz: ");
        int a = scanner.nextInt();
        StringBuilder toplam = new StringBuilder();

        for (int i = 1; i < a+1; i++) {

            System.out.println("Davay "+(i+1)+"chi sonni kiriting to'plamga: ");
            int b=scanner.nextInt();

            if (i % 2 != 0) {
                if (min1 > min2) {
                    if (min1 > b) {
                        min1 = b;
                    }
                } else {
                    if (min2 > b) {
                        min2 = b;
                    }
                }
            }else {
                if (min2 > min1) {
                    if (min2 > b) {
                        min2 = b;
                    }
                } else {
                    if (min1 > b) {
                        min1 = b;
                    }
                }
            }
            toplam.append(b).append(" ");
        }
        if (min1> min2) {
            int b = min1;
            min1 = min2;
            min2 = b;
        }

        System.out.println("To'plam: "+toplam+"\nMin1: " + min1 + ", Min2: " + min2);
    }

    public static void main(String[] args) {
        getMinMax();
    }
}
