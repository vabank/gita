package bMinMax;
// Muallif: Aziz Ismoilov
// Sana: 20.01.2021
// Maqsad: Kiritilgan to'plamdagi eng kichik musbat sonni topish

import java.util.Scanner;

public class Begin17 {

    static void getMinMax() {

        int max = 0, min = Integer.MAX_VALUE;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta sonli to'plam yaratmoqchisiz: ");
        int a = scanner.nextInt();
        StringBuilder toplam = new StringBuilder();

        for (int i = 0; i < a; i++) {

            System.out.println("Davay "+(i+1)+"chi sonni kiriting to'plamga: ");
            int b=scanner.nextInt();

            if (b>0&&min > b) {
                min = b;
            }else {
                min = 0;
            }
            toplam.append(b).append(" ");
        }
        System.out.println(min);


    }

    public static void main(String[] args) {
        getMinMax();
    }
}
