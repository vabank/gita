package bMinMax;
// Muallif: Aziz Ismoilov
// Sana: 20.01.2021
// Maqsad: Kiritilgan to'plamdagi eng kichik son va uning pozitsiyasini topppish

import java.util.Scanner;

public class Begin13 {

    static void getMinMax() {

        int min=Integer.MAX_VALUE, position = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta sonli to'plam yaratmoqchisiz: ");
        int a = scanner.nextInt();
        StringBuilder toplam = new StringBuilder();

        for (int i = 0; i < a; i++) {

            System.out.println("Davay "+(i+1)+"chi sonni kiriting to'plamga: ");
            int b=scanner.nextInt();

            if (min > b) {
                min = b;
                position = i+1;
            }

            toplam.append(b).append(" ");
        }
        System.out.println("Umumiy to'plam: "+toplam+"\nPositsiiyasi: "+position+", Min: "+min);
    }

    public static void main(String[] args) {
        getMinMax();
    }
}
