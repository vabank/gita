package bMinMax;
// Muallif: Aziz Ismoilov
// Sana: 20.01.2021
// Maqsad: Kiritilgan to'plamdagi ekstremalni topish oxirgi uchragan

import java.util.Scanner;

public class Begin16 {

    static void getMinMax() {

        int max = 0, min = Integer.MAX_VALUE, position = 0, positionMax = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta sonli to'plam yaratmoqchisiz: ");
        int a = scanner.nextInt();
        StringBuilder toplam = new StringBuilder();

        for (int i = 0; i < a; i++) {

            System.out.println("Davay "+(i+1)+"chi sonni kiriting to'plamga: ");
            int b=scanner.nextInt();
            if (max <= b) {
                max = b;
                positionMax = i+1;
            }
            if (min >= b) {
                min = b;
                position = i+1;
            }


            toplam.append(b).append(" ");
        }
        if (position > positionMax) {
            System.out.println("Umumiy to'plam: "+toplam+"\nOxirgi uchragan ekstremal: "+min+
                    " Pozitsiyasi: "+position);
        }else {
            System.out.println("Umumiy to'plam: "+toplam+"\nOxirgi uchragan ekstremal: "+max+
                    " Pozitsiyasi: "+positionMax);
        }

    }

    public static void main(String[] args) {
        getMinMax();
    }
}
