package LeetCode.com.first;

//1221. Split a String in Balanced Strings

public class BalancedStringSplit {

    public static int balancedStringSplit(String s) {
        int result = 0, countR=0, countL=0;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'R') {
                countR++;
            } else {
                countL++;
            }

            if (countL == countR) {
                result++;
                countL = 0;
                countR = 0;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(balancedStringSplit("RLRRRLLRLL"));
    }
}
