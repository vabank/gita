package LeetCode.com.first;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//1431
//Kids With the Greatest Number of Candies

public class KidsWithCandies {
    public static List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
        List<Boolean> list = new ArrayList<>();
        int max =0;
        for (int candy : candies) {
            if (candy > max) {
                max = candy;
            }
        }
        for (int candy : candies) {
            list.add(candy + extraCandies >= max);
        }
        return list;

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] num = new int[5];
        for (int i = 0; i < num.length; i++) {
            System.out.println(i+1+" chi elementni kiriting");
            num[i] = scanner.nextInt();
        }
        System.out.println(kidsWithCandies(num, 1));
    }
}
