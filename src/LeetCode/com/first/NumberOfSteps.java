package LeetCode.com.first;

//1342
//Number of Steps to Reduce a Number to Zero

public class NumberOfSteps {

    public static int numberOfSteps (int num) {
        int count = 0;
        while (num != 0) {
            if (num % 2 == 0) {
                num = num / 2;
            } else {
                num = (num - 1);
            }
            count++;
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println(numberOfSteps(8));
    }
}
