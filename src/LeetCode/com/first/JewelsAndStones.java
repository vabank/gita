package LeetCode.com.first;

//771
//Jewels and Stones

import java.util.Scanner;

public class JewelsAndStones {

    public static int numJewelsInStones(String jewels, String stones) {

        int result = 0;

        for (int i = 0; i < jewels.length(); i++) {
            for (int j = 0; j < stones.length(); j++) {
                if (jewels.charAt(i) == stones.charAt(j)) {
                    result++;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("jewels = ");
        String jewels = scanner.nextLine();
        System.out.println("stones = ");
        String stones = scanner.nextLine();

        System.out.println(numJewelsInStones(jewels, stones));

    }
}
