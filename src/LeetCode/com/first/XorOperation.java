package LeetCode.com.first;

//1486
//XOR Operation in an Array

public class XorOperation {
    public static int xorOperation(int n, int start) {
        int result=0;
        int[] nums=new int[n];
        for(int i=0; i<n; i++){
            nums[i]=start+2*i;
        }
        result=nums[0];
        for(int i=0; i<n-1; i++){
            result=result^nums[i+1];
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(xorOperation(5, 0));
    }
}
