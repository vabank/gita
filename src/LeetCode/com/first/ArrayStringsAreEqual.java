package LeetCode.com.first;

//1662. Check If Two String Arrays are Equivalent

public class ArrayStringsAreEqual {

    public static boolean arrayStringsAreEqual(String[] word1, String[] word2) {
        String one="", second = "";
        for (String value : word1) {
            one = one + value;
        }

        for (String s : word2) {
            second = second + s;
        }

        return one.equals(second);
    }

    public static void main(String[] args) {
        String[] one = new String[]{"ab", "c"};
        String[] second = new String[]{"ab", "c"};
        System.out.println(arrayStringsAreEqual(one, second));
    }
}
