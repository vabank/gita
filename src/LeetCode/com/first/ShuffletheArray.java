package LeetCode.com.first;

import java.util.Arrays;
import java.util.Scanner;

//1470
//Shuffle the Array

public class ShuffletheArray {

    public static int[] shuffle(int[] nums, int n) {
        int[] result = new int[nums.length];
        int pos = 0;

        for (int i = 0; i < n; i++) {
            result[pos] = nums[i];
            pos++;
            result[pos] = nums[i+n];
            pos++;
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Massiv nechta elementdan tashkil topsin: ");
        int m = scanner.nextInt();
        int[] myArray = new int[m];

        for (int j = 0; j < m; j++) {
            System.out.println(j+1+" chi elementni kiriting brat");
            myArray[j] = scanner.nextInt();
        }
        System.out.println("n = ");
        int n = scanner.nextInt();
        System.out.println(Arrays.toString(shuffle(myArray, n)));
    }
}
