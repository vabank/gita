package LeetCode.com.first;

//1108. Defanging an IP Address

public class IPAddress {

    public static String derangedIpAddress(String address) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < address.length(); i++) {
            char a = address.charAt(i);
            if (a == '.') {
                result.append("[.]");
            } else {
                result.append(address.charAt(i));
            }
        }
        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println(derangedIpAddress("1.10.1.1"));
    }
}
