package LeetCode.com.first;

//1528
//Shuffle String

public class ShuffleString {

    public String restoreString(String s, int[] indices) {
        String result = "";
        char[] myArray = new char[indices.length];
        for (int i = 0; i < indices.length; i++) {
            myArray[indices[i]] = s.charAt(i);
        }
        for (int i = 0; i < indices.length; i++) {
            result = result + myArray[i];
        }

        return result;
    }
}
