package LeetCode.com.first;

import java.util.Arrays;

//1389. Create Target Array in the Given Order

public class CreateTargetArray {

    public static int[] createTargetArray(int[] nums, int[] index) {
        int last = 0;
        int[] ret = new int[nums.length];

        for (int i = 0; i < index.length; i++) {

            if (index[i] == last) {
                ret[index[i]] = nums[i];
                last = index[i] + 1;
            } else {
                for (int j = last - 1; j >= index[i]; j--) {
                    ret[j + 1] = ret[j];
                }
                ret[index[i]] = nums[i];
                last++;
            }
        }
        return ret;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{0,1,2,3,4};
        int[] index = new int[]{0, 1, 2, 2, 1};
        System.out.println(Arrays.toString(createTargetArray(nums, index)));
    }
}
