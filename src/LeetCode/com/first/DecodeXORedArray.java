package LeetCode.com.first;

import java.util.Arrays;
import java.util.Scanner;

//1720. Decode XORed Array

public class DecodeXORedArray {
    public static int[] decode(int[] encoded, int first) {

        int[] result = new int[encoded.length+1];
        result[0] = first;
        for (int i = 0; i < encoded.length; i++) {
            result[i+1] = result[i] ^ encoded[i];
        }


        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Massiv nechta elementdan tashkil topsin: ");
        int m = scanner.nextInt();
        int[] myArray = new int[m];

        for (int j = 0; j < m; j++) {
            System.out.println(j+1+" chi elementni kiriting brat");
            myArray[j] = scanner.nextInt();
        }

        System.out.println("Birinchi elementni kiriting: ");
        int first = scanner.nextInt();
        System.out.println(Arrays.toString(decode(myArray,first)));
    }
}
