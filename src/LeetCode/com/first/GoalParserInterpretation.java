package LeetCode.com.first;

import java.util.Scanner;

//1678. Goal Parser Interpretation

public class GoalParserInterpretation {
    public static String interpret(String command) {
        String result = "";
        for (int i = 0; i < command.length(); i++) {
            switch (command.charAt(i)) {
                case 'G':
                    result = result + "G";
                    break;
                case '(':
                    if (command.charAt(i + 1) == ')') {
                        result = result + "o";
                    }else {
                        result = result + "al";
                    }
                    break;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter String");
        String command = scanner.nextLine();
        System.out.println(interpret(command));
    }
}
