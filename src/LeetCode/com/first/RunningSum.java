package LeetCode.com.first;

import java.util.Arrays;

// 1480. Running Sum of 1d Array

class RunningSum {

    public static int[] runningSum(int[] nums) {

        int prev=0;
        for(int i=0;i<nums.length;i++) {
            nums[i]=nums[i]+prev;
            prev=nums[i];
        }
        return nums;

    }

    public static void main(String[] args) {
        int[] num = new int[5];
        for (int i = 0; i < num.length; i++) {
            num[i] = i + 1;
        }
        System.out.println(Arrays.toString(num) +"\n\nJavobi: "+Arrays.toString(runningSum(num)));
    }
}