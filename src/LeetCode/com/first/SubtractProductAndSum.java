package LeetCode.com.first;

//1281
//Subtract the Product and Sum of Digits of an Integer

public class SubtractProductAndSum {

    public static int subtractProductAndSum(int n) {
        int  product=1, sum = 0;
        while (n > 0) {
            product = product * (n % 10);
            sum = sum + (n % 10);
            n = n / 10;
        }
        return product - sum;
    }

    public static void main(String[] args) { System.out.println(subtractProductAndSum(4421));
    }
}
