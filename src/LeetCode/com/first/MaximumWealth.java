package LeetCode.com.first;




import java.util.Scanner;

//1672. Richest Customer Wealth

public class MaximumWealth {

    public static  int maximumWealth(int[][] accounts) {
        int sum = 0;
        int max = 0;

        for(int i = 0; i < accounts.length; i++){
            sum = 0;
            for(int j = 0; j < accounts[0].length; j++){
                sum = sum + accounts[i][j];
            }

            if(sum > max)
                max = sum;

        }
        return max;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka mxn o'lchamli array yasavommim m=");
        int m = scanner.nextInt();
        System.out.println("Oka mxn o'lchamli array yasavommim n=");
        int n = scanner.nextInt();
        int[][] myArray = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.println(i+" "+j+" chi elementni kiriting brat");
                myArray[i][j] = scanner.nextInt();
            }
        }
        System.out.println(maximumWealth(myArray));
    }
}
