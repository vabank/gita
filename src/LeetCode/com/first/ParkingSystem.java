package LeetCode.com.first;

//1603
//Design Parking System

public class ParkingSystem {

    int[] openSpace = new int[3];
    public ParkingSystem(int big, int medium, int small) {

        openSpace[0] = big;
        openSpace[1] = medium;
        openSpace[2] = small;
    }

    public boolean addCar(int carType) {
        if(openSpace[carType-1]>0)
        {
            openSpace[carType-1]--;
            return true;
        }
        else
            return false;
    }

    public static void main(String[] args) {
        ParkingSystem parkingSystem = new ParkingSystem(1, 1, 0);
        System.out.println(parkingSystem.addCar(2));
    }
}
