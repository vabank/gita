package LeetCode.com.first;

//512
//Number of Good Pairs

import java.util.Scanner;

public class NumberOfGoodNumber {

    public static int numIdenticalPairs(int[] nums) {
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (nums[i] == nums[j] && i < j) {
                    result++;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Massiv nechta elementdan tashkil topsin: ");
        int m = scanner.nextInt();
        int[] myArray = new int[m];

        for (int j = 0; j < m; j++) {
                System.out.println(j+1+" chi elementni kiriting brat");
                myArray[j] = scanner.nextInt();
        }
        System.out.println(numIdenticalPairs(myArray));
    }
}
