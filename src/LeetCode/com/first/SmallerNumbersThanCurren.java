package LeetCode.com.first;

import java.util.Arrays;
import java.util.Scanner;

//1365
//How Many Numbers Are Smaller Than the Current Number

public class SmallerNumbersThanCurren {
    public static int[] smallerNumbersThanCurrent(int[] nums) {
        int[] result = new int[nums.length];
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (nums[i] > nums[j]) {
                    count++;
                }
            }
            result[i] = count;
            count = 0;
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Massiv nechta elementdan tashkil topsin: ");
        int m = scanner.nextInt();
        int[] myArray = new int[m];

        for (int j = 0; j < m; j++) {
            System.out.println(j+1+" chi elementni kiriting brat");
            myArray[j] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(smallerNumbersThanCurrent(myArray)));
    }
}
