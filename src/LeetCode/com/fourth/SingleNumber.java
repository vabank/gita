package LeetCode.com.fourth;

//136
//Single Number

public class SingleNumber {

    public static int singleNumber(int[] nums) {
        int result = 0;

        for(int n : nums)
            result ^= n;

        return result;
    }

    public static void main(String[] args) {
        System.out.println(singleNumber(new int[]{2,2,1,1,3}));
    }

}
