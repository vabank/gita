package LeetCode.com.fourth;

///1346. Check If N and Its Double Exist

public class CheckIfExist {

    public static boolean checkIfExist(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) { {
            for (int j = 0; j < arr.length; j++) {
                if (i != j) {
                    if (arr[i] * 2 == arr[j] || arr[j] * 2 == i) {
                        return true;
                    }
                }
            }
           }
        }
            return false;
    }

    public static void main(String[] args) {
        System.out.println(checkIfExist(new int[]{10, 2, 5, 4}));
    }
}
