package LeetCode.com.fourth;

//58
//Length of Last Word

public class LengthOfLastWord {
    public static int lengthOfLastWord(String s) {
        String[] a=s.split(" ");

        return a.length >= 1 ? a[a.length - 1].length() : 0;
    }

    public static void main(String[] args) {
        System.out.println(lengthOfLastWord("Hello World"));
    }
}
