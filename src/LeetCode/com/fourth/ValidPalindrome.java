package LeetCode.com.fourth;

//680
//Valid Palindrome II

public class ValidPalindrome {

    public static boolean validPalindrome(String s) {
        for(int i = 0, j = s.length() -1; i <= j; i++, j--)
            if (s.charAt(i) != s.charAt(j))
                return isPalindrome(s.substring(i, j)) || isPalindrome(s.substring(i + 1, j + 1));

        return true;
    }

    public static boolean isPalindrome(String s){
        for(int i = 0, j = s.length() - 1; i <= j; i++, j--)
            if(s.charAt(i) != s.charAt(j))
                return false;

        return true;
    }

    public static void main(String[] args) {
        System.out.println(validPalindrome("abchba"));
    }

}
