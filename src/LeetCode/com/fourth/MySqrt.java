package LeetCode.com.fourth;

//69
//Sqrt(x)

public class MySqrt {

    public static int mySqrt(int x) {
        if (x < 2) return x;
        int left = 0, right = x / 2;
        while (left <= right) {
            double mid = left + (right - left) / 2;
            double squared = mid * mid;
            if (squared == x) return (int) mid;
            else if (squared > x) right = (int) mid - 1;
            else left = (int)mid + 1;
        }
        return right;
    }

    public static void main(String[] args) {
        System.out.println(mySqrt(9));

    }
}
