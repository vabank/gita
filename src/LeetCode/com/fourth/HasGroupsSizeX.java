package LeetCode.com.fourth;

import java.util.HashMap;

public class HasGroupsSizeX {

    public static boolean hasGroupsSizeX(int[] deck) {
        HashMap<Integer, Integer> map=new HashMap<>();
        for (int k : deck) {
            if (!map.containsKey(k)) map.put(k, 1);
            else map.put(k, map.get(k) + 1);
        }
        for (int i = 0; i < map.size(); i++) {
            System.out.print(map.get(i));
        }
        int lim=Integer.MAX_VALUE;

        for(int i : map.keySet())
            lim=Math.min(lim, map.get(i));
        boolean flag=true;
        for(int i=2;i<=lim;i++) {
            for(int j : map.keySet()) {
                if(map.get(j)%i!=0)
                {
                    flag=false;
                    break;
                }
            }
            if(flag) return true;
            flag=true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(hasGroupsSizeX(new int[]{0,0,0,1,1,1,2,2,2}));
    }
}
