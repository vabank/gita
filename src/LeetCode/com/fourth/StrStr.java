package LeetCode.com.fourth;


//28
//Implement strStr()

public class StrStr {

    public static  int strStr(String haystack, String needle) {

        if(needle == null || needle.equals(""))
            return 0;
        else if(!haystack.contains(needle))
            return -1;
        else{
            for(int i = 0; i<haystack.length();i++){
                String sub = haystack.substring(i, i+needle.length());
                if(sub.equals(needle)){
                    return i;
                }
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(strStr("mississipi", "issipi"));
    }
}
