package LeetCode.com.fourth;

//14
//Longest Common Prefix

public class LongestCommonPrefix {

    public static String longestCommonPrefix(String[] strs) {
        if(strs==null || strs.length==0) return "";
        String str= strs[0];
        for(int i=1;i<strs.length;i++){
            while(strs[i].indexOf(str)!=0){
                str=str.substring(0, str.length()-1);
            }
        }
        return str;
    }

    public static void main(String[] args) {
        System.out.println(longestCommonPrefix(new String[]{"flower", "flow", "flight"}));
        System.out.println("flight".indexOf("fl"));

    }
}
