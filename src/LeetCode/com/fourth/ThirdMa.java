package LeetCode.com.fourth;

//414
//Third Maximum Number

public class ThirdMa {

    public static  int thirdMax(int[] nums) {

        int arr_size = nums.length;

        int first = nums[0];
        for (int i = 1; i < arr_size ; i++)
            if (nums[i] > first)
                first = nums[i];

        if (arr_size < 3)
        {
            return first;
        }

        int second = Integer.MIN_VALUE;
        for (int num : nums)
            if (num > second && num < first)
                second = num;

        int count=0;
        int third = Integer.MIN_VALUE;
        for (int num : nums)
            if (num >= third && num < second) {
                third = num;
                count++;
            }
        if(count>0)
            return third;
        else
            return first;

    }

    public static void main(String[] args) {
        int[] a = new int[]{1, 2, 3, 1, 23, 12};
        System.out.println(thirdMax(a));

    }
}
