package LeetCode.com.fourth;

//941
//Valid Mountain Array

public class ValidMountainArray {

    public static boolean validMountainArray(int[] A) {
        int len = A.length;
        if(len < 3) return false;
        for(int i = 1; i < len - 1; i++) {
            if(A[i] > A[i - 1] && A[i] > A[i + 1]) {
                int left = i, right = i;
                while(left > 0 && A[left] > A[left - 1])
                    left--;
                while(right < len - 1 && A[right] > A[right + 1])
                    right ++ ;
                if(left == 0 && right == len - 1)
                    return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(validMountainArray(new int[]{1, 2, 3, 4, 5, 4, 3}));
    }
}
