package LeetCode.com.fourth;

import java.util.ArrayList;
import java.util.Arrays;


//434
//Number of Segments in a String

public class CountSegments {

    public static int countSegments(String s) {
        int segmentCount = 0;

        for (int i = 0; i < s.length(); i++) {
            if ((i == 0 || s.charAt(i-1) == ' ') && s.charAt(i) != ' ') {
                segmentCount++;
            }
        }

        return segmentCount;
    }

    public static void main(String[] args) {
        System.out.println(countSegments("Hello world"));
    }

}
