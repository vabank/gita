package LeetCode.com.fourth;

//507. Perfect Number

public class CheckPerfectNumber {

    public static boolean checkPerfectNumber(int num) {
        if (num <= 1) {
            return false;
        }
        int sum = 0;
        for (int i = 1; i * i <= num; i++) {
            if (num % i == 0) {
                sum += i+num / i;
            }
        }
        return sum - num == num;
    }

    public static void main(String[] args) {
        System.out.println(checkPerfectNumber(1));
    }
}
