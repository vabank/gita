package LeetCode.com.fourth;

///1768
//Merge Strings Alternately

public class MergeAlternately {
    public static String mergeAlternately(String w1, String w2) {
        int n = w1.length(), m = w2.length(), i = 0;
        StringBuilder res = new StringBuilder();
        while (i < n || i < m) {
            if (i < w1.length())
                res.append(w1.charAt(i));
            if (i < w2.length())
                res.append(w2.charAt(i));
            i++;
        }
        return res.toString();
    }

    public static void main(String[] args) {
        System.out.println(mergeAlternately("abc", "psnj"));
    }
}
