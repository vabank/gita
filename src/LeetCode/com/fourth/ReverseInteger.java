package LeetCode.com.fourth;

//	7
//Reverse Integer

public class ReverseInteger {
    public static int reverse(int x) {

        boolean neg = ( x<0 );
        long y = x;
        if( neg ) y = -y;
        long reversed = Long.parseLong( ( new StringBuilder( "" + y ) ).reverse().toString() );
        if( neg ) reversed = -reversed;
        if( reversed > Integer.MAX_VALUE || reversed < Integer.MIN_VALUE ) return 0;
        return (int) reversed;

    }

    public static void main(String[] args) {
        System.out.println(reverse(-234));
    }
}
