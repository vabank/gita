package LeetCode.com.fourth;

//168. Excel Sheet Column Title

public class ConvertToTitle {

    public static String convertToTitle(int n) {
        if (n <= 26) return (char)('A' + n - 1) + "";
        if (n % 26 != 0) return convertToTitle((n / 26)) + (char)('A' + (n % 26) - 1);
        else return convertToTitle((n / 26) - 1) + "Z";
    }

    public static void main(String[] args) {
        System.out.println(convertToTitle(27*26));
    }
}
