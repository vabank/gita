package LeetCode.com.fourth;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

//859
//Buddy Strings

public class BuddyStrings {
    public static boolean buddyStrings(String A, String B) {
        boolean result = false;
        if(A.length()!=B.length())
            return false;
        if(A.equals(B)){

            HashSet<Character> h= new HashSet<Character>();
            for(char c: A.toCharArray())
                h.add(c);
            return h.size() < A.length();
        }
        List<Integer> res= new ArrayList<>();

        for(int i=0; i<A.length(); i++) {
            if(A.charAt(i)!= B.charAt(i))
                res.add(i);
        }
        for (int i = 0; i < res.size() / 2; i++) {
          result= res.size() == 2 && A.charAt(res.get(0)) == B.charAt(res.get(1)) &&
                    B.charAt(res.get(0)) == A.charAt(res.get(1));
        }
        return result;

    }

    public static void main(String[] args) {
        System.out.println(buddyStrings("ab", "ba"));
    }

}
