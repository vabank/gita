package LeetCode.com.fourth;

import java.util.Arrays;

//	204
//Count Primes

public class CountPrimes {

    public static int countPrimes(int n) {
        boolean[] bol = new boolean[n+1];
        Arrays.fill(bol,true);
        int c = 0;
        for(int i = 2; i*i <= n; i++) {
            if(bol[i]) {
                for(int j = i*i; j <= n; j+=i) {
                    bol[j] = false;
                }
            }
        }
        for(int i = 2; i < n; i++) {
            if(bol[i]) c++;
        }
        return c;
    }

    public static void main(String[] args) {
        System.out.println(countPrimes(10));
    }

}
