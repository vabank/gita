package LeetCode.com.third;


//	1450
//Number of Students Doing Homework at a Given Time

public class BusyStudent {

    public int busyStudent(int[] startTime, int[] endTime, int queryTime) {
        int result = 0;
        for (int i = 0; i < startTime.length; i++) {
            if (startTime[i]<=queryTime&&queryTime<=endTime[i]) {
                result++;
            }
        }
        return result;
    }
}
