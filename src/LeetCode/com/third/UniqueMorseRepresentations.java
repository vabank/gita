package LeetCode.com.third;

import java.util.HashSet;
import java.util.Set;

//	804
//Unique Morse Code Words

public class UniqueMorseRepresentations {

    public static int uniqueMorseRepresentations(String[] words)
    {
        String[] d = {".-", "-...", "-.-.", "-..",".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--",
                "-.", "---", ".--.","--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};

        Set<String> set = new HashSet();
        for (String word : words)
        {
            StringBuilder s = new StringBuilder();
            for (char c : word.toCharArray()) s.append(d[c - 'a']);
            set.add(s.toString());
        }

        return set.size();
    }

    public static void main(String[] args) {
        String[] s = new String[]{"gin", "zen", "gig", "msg"};
        System.out.println(uniqueMorseRepresentations(s));
    }
}
