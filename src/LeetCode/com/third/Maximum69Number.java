package LeetCode.com.third;

//1323
//Maximum 69 Number

public class Maximum69Number {
    public static  int maximum69Number(int num) {
        char[] chars = Integer.toString(num).toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '6') {
                chars[i] = '9';
                break;
            }
        }
        return Integer.parseInt(new String(chars));
    }

    public static void main(String[] args) {
        System.out.println(maximum69Number(6669));
    }
}
