package LeetCode.com.third;

//1572
//Matrix Diagonal Sum

public class DiagonalSum {

    public int diagonalSum(int[][] mat) {
        int sum = 0;
        int start = 0, end=mat.length-1;
        while(start<=end){
            if(start == end){
                sum += mat[start][end];
            }
            else{
                sum += mat[start][start];
                sum += mat[start][end];
                sum += mat[end][start];
                sum += mat[end][end];
            }
            start++;
            end--;
        }
        return sum;
    }
}
