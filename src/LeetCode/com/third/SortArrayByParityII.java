package LeetCode.com.third;

import java.util.Arrays;

//922
//Sort Array By Parity II

public class SortArrayByParityII {

    public static int[] sortArrayByParityII(int[] A) {
        int even = 0, odd = 1;

        for (int i = 0; i < A.length;) {
            if (i % 2 == 0) {
                if (A[i] % 2 == 0) {
                    i++;
                } else {
                    int temp = A[odd];
                    A[odd] = A[i];
                    A[i] = temp;

                    odd += 2;
                }
            } else {
                if (A[i] % 2 ==0) {
                    int temp = A[even];
                    A[even] = A[i];
                    A[i] = temp;

                    even += 2;
                } else {
                    i++;
                }
            }
        }

        return A;
    }

    public static void main(String[] args) {

        int[] myArray = new int[]{4,2,5,7};
        System.out.println(Arrays.toString(sortArrayByParityII(myArray)));
    }

}
