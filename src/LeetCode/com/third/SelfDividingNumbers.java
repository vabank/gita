package LeetCode.com.third;

import java.util.ArrayList;
import java.util.List;

//728
//Self Dividing Numbers

public class SelfDividingNumbers {
    public static List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> result = new ArrayList<>();
        int butun = 0, qoldiq = 0, currentNum = 0, start = left, nechixonali = 0;
        boolean isSelfDivisible = false;
        for (int i = start; i <= right; i++) {
            nechixonali=nechiXonali(left);
            if (nechixonali == 1&&left%10!=0) {
                result.add(left);
            } else if (nechixonali > 1) {
                currentNum = left;
                for (int j = 0; j < nechixonali; j++) {
                    butun = currentNum / 10;
                    qoldiq = currentNum % 10;
                    if (qoldiq != 0 && left % qoldiq == 0) {
                        isSelfDivisible = true;
                    } else {
                        isSelfDivisible = false;
                        break;
                    }
                    currentNum = currentNum / 10;
                }
                if (isSelfDivisible) {
                    result.add(left);
                    isSelfDivisible = false;
                }
            }
            left++;
        }
        return result;
    }

    public static int nechiXonali(int num) {
        int result = 0, qoldiq=0;
        while (num != 0) {
            qoldiq = num % 10;
            if (qoldiq != 0) {
                result++;
            }
            num = num / 10;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(selfDividingNumbers(1, 22));
        System.out.println(nechiXonali(13));
    }
}
