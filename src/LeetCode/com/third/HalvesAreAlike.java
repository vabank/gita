package LeetCode.com.third;


//1704
//Determine if String Halves Are Alike


public class HalvesAreAlike {

    public static boolean halvesAreAlike(String s) {
        boolean result = true;
        int firstSum = 0;
        int secondSum = 0;
        int[] firstChar = new int[s.length() / 2];
        int[] secondChar = new int[s.length() / 2];
        int myLength = s.length()/2;
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i)=='A'||s.charAt(i)=='E'||s.charAt(i)=='I'||s.charAt(i)=='O'||s.charAt(i)=='U'||
                    s.charAt(i)=='a'||s.charAt(i)=='e'||s.charAt(i)=='i'||s.charAt(i)=='o'||s.charAt(i)=='u') {
                firstChar[i] = s.charAt(i);
            }
            if (s.charAt(myLength)=='A'||s.charAt(myLength)=='E'||s.charAt(myLength)=='I'||s.charAt(myLength)=='O'||s.charAt(myLength)=='U'||
                    s.charAt(myLength)=='a'||s.charAt(myLength)=='e'||s.charAt(myLength)=='i'||s.charAt(myLength)=='o'||s.charAt(myLength)=='u') {
                secondChar[i] = s.charAt(myLength);
            }
            myLength++;
        }
        for (int i = 0; i < s.length() / 2; i++) {
            if (firstChar[i] != 0) {
                firstSum++;
            }
            if (secondChar[i] != 0) {
                secondSum++;
            }
        }
        for (int i = 0; i < s.length() / 2; i++) {
            System.out.print(firstChar[i]+" ");
        }
        for (int i = 0; i < s.length() / 2; i++) {
            System.out.print(secondChar[i]+" ");
        }
        return firstSum == secondSum;
    }

    public static void main(String[] args) {
        System.out.println(halvesAreAlike("sbookA"));
    }

}
