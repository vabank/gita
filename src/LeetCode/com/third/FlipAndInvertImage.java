package LeetCode.com.third;

import java.util.Arrays;

//832
//Flipping an Image

public class FlipAndInvertImage {

    public static int[][] flipAndInvertImage(int[][] A) {
        int[][] result = new int[A.length][A[0].length];
        int start = 0, end = 0, tmp = 0;
        for (int i = 0; i < A.length; i++) {
            end = A.length-1;
            start=0;
            for (int j = 0; j < A.length; j++) {
                if (A.length % 2 != 0) {
                    if (start != end) {
                        tmp = A[i][j];
                        A[i][j] = A[i][end];
                        A[i][end] = tmp;
                        start++;
                        end--;
                    }
                } else {
                    if (start < end) {
                        tmp = A[i][j];
                        A[i][j] = A[i][end];
                        A[i][end] = tmp;
                        start++;
                        end--;
                    }
                }


            }
        }
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A.length; j++) {
                A[i][j] = (A[i][j] == 0) ? 1 : 0;
            }
        }
        return A;
    }

    public static void main(String[] args) {
        int[][] A = {{1,1,0,0},{1,0,0,1},{0,1,1,1},{1,0,1,0}};
        System.out.println(Arrays.deepToString(flipAndInvertImage(A)));
    }
}
