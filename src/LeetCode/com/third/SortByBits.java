package LeetCode.com.third;

import java.util.Arrays;

//1356
//Sort Integers by The Number of 1 Bits

public class SortByBits {

    public static int[] sortByBits(int[] myArray) {

        for (int i = 0; i < myArray.length; i++) {
            for (int j = i + 1; j < myArray.length; j++) {
                int tmp = 0;
                if (Integer.bitCount(myArray[i]) > Integer.bitCount(myArray[j])) {
                    tmp = myArray[i];
                    myArray[i] = myArray[j];
                    myArray[j] = tmp;
                } else if (Integer.bitCount(myArray[i]) == Integer.bitCount(myArray[j])) {
                    if (myArray[i] >myArray[j]) {
                        tmp = myArray[i];
                        myArray[i] = myArray[j];
                        myArray[j] = tmp;
                }
            }
        }
    }
        return myArray;
    }
    public static void main(String[] args) {
        int[] arr = new int[]{0,1,2,3,4,5,6,7,8};
        System.out.println(Arrays.toString(sortByBits(arr)));
    }

}
