package LeetCode.com.third;

import java.util.ArrayList;
import java.util.List;

//389
//Find the Difference

public class FindTheDifference {

    public static char findTheDifference(String s, String t) {
        if(s == null) return '\0';
        int val = t.charAt(t.length()-1);
        for(int i=0; i < s.length(); i++)
            val = val - s.charAt(i) + t.charAt(i);
        return (char) val;
    }
    public static void main(String[] args) {
        System.out.println(findTheDifference("abcd", "aedcb"));
    }

}
