package LeetCode.com.third;

//	1748
//Sum of Unique Elements

public class SumOfUnique {

    public static int sumOfUnique(int[] nums) {
        int result = 0, count = 0;
        for (int i = 0; i < nums.length; i++) {
            count = 0;
            for (int j = 0; j < nums.length; j++) {
                if (j != i) {
                    if (nums[i] ==nums[j]){
                        count++;
                    }
                }
            }
            if (count == 0) {
                result = result + nums[i];
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] answer = new int[]{1,2, 2, 3,34,1};
        System.out.println(sumOfUnique(answer));
    }

}
