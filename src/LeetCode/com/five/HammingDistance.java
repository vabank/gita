package LeetCode.com.five;

//461
//Hamming Distance

public class HammingDistance {
    public int hammingDistance(int x, int y) {
        return Integer.bitCount(x ^ y);
    }
}
