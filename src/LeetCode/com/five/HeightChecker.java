package LeetCode.com.five;

import java.util.PriorityQueue;

//1051
//Height Checker

public class HeightChecker {
    public int heightChecker(int[] heights) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();

        for (int n : heights) {
            pq.add(n);
        }

        int res = 0;
        for (int n : heights) {
            if (n != pq.poll()) res++;
        }

        return res;
    }
}
