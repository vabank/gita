package LeetCode.com.five;

import java.util.Arrays;

public class SortString {

    public static String sortString(String s) {
        int[] dict = new int[26];
        int count = s.length();

        for(char c : s.toCharArray()) {
            dict[c - 'a']++;
        }

        System.out.println(Arrays.toString(dict));

        StringBuilder sb = new StringBuilder();

        while(count > 0) {
            for(int i = 0; i < 26; i++) {
                if(dict[i] > 0) {
                    sb.append((char) (i + 'a'));
                    dict[i]--;
                    count--;
                }
            }

            for(int i = 25; i >= 0; i--) {
                if(dict[i] > 0) {
                    sb.append((char) (i + 'a'));
                    dict[i]--;
                    count--;
                }
            }
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(sortString("pos"));
    }
}
