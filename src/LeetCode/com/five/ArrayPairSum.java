package LeetCode.com.five;

import java.util.Arrays;

//561. Array Partition I

public class ArrayPairSum {
    public static int arrayPairSum(int[] nums) {
        int sum = 0;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i += 2) {
            sum += Math.min(nums[i], nums[i+1]);
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(arrayPairSum(new int[]{1, 2, 3, 4}));
    }
}
