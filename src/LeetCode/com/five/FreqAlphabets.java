package LeetCode.com.five;



public class FreqAlphabets {

    public static String freqAlphabets(String s) {
        char[] r= s.toCharArray();
        StringBuilder str = new StringBuilder();
        for(int i=0 ; i<r.length;i++){
            if(i<r.length-2 && r[i+2] == '#'){
                str.append((char) ((((r[i] - '1') * 10) + (r[i+1] -'0')) + 'j'));
                System.out.println(str);
                i+=2;
            }
            else{
                str.append((char) ('a' + (r[i] - '1')));
                System.out.println(str);
            }
        }
        return str.toString();
    }

    public static void main(String[] args) {
        System.out.println(freqAlphabets("10#11#12"));
    }
}
