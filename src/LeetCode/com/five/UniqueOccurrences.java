package LeetCode.com.five;

import java.util.HashMap;
import java.util.HashSet;

//1207
//Unique Number of Occurrences

public class UniqueOccurrences {
    public static boolean uniqueOccurrences(int[] arr) {
        HashMap<Integer, Integer> map = new HashMap();
        for (int num : arr) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        System.out.println(map.values());
        HashSet<Integer> set = new HashSet(map.values());
        System.out.println(set);// hash sets only keep unique values
        return map.size() == set.size();
    }

    public static void main(String[] args) {
        System.out.println(uniqueOccurrences(new int[]{1,2,2,1,1,3}));
    }
}
