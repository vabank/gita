package LeetCode.com.five;

//1534
//Count Good Triplets

public class CountGoodRectangles {
    public static int countGoodRectangles(int[][] rectangles) {

        int maxLen = 0;
        int numSquares = 0; // what we'll return

        for (int[] arr : rectangles) {

            int curLen = Math.min(arr[0], arr[1]);

            if (curLen == maxLen) {
                ++numSquares;
            } else if (curLen > maxLen) {
                maxLen = curLen;
                numSquares = 1;
            } else { // curLen < maxLen
                // do nothing
            }
        }

        return numSquares;
    }

    public static void main(String[] args) {
        System.out.println(countGoodRectangles(new int[][]{{2, 3}, {3, 4}, {5, 3}, {6, 3}}));
    }
}
