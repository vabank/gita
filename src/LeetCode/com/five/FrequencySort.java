package LeetCode.com.five;

import java.util.*;
import java.util.stream.Collectors;


//1636
//Sort Array by Increasing Frequency

public class FrequencySort {
    public static int[] frequencySort(int[] nums) {
        Map<Integer, Integer> count = new HashMap<>();

        for (int num : nums) {
            count.put(num, count.getOrDefault(num, 0) + 1);
            int a = count.getOrDefault(num, 0);
        }


        List<Integer> sortedNumbers = count.keySet().stream()
                .sorted(
                        Comparator.comparing(i -> count.get(i))
                                .thenComparing(i -> -(int)i)
                )
                .collect(Collectors.toList());

        System.out.println(sortedNumbers);
        int[] result = new int[nums.length];
        int idx = 0;
        for(int number: sortedNumbers) {
            for(int i = 0 ; i < count.get(number) ; i++) {
                result[idx++] = number;
            }
        }
        return result;
    }



    public static void main(String[] args) {
        System.out.println(Arrays.toString(frequencySort(new int[]{2,3,1,3,2})));
    }
}
