package LeetCode.com.five;

import java.util.Arrays;

//1374. Generate a String With Characters That Have Odd Counts

public class GenerateTheString {
    public static  String generateTheString(int n) {
        char[] characters = new char[n];
        Arrays.fill(characters, 'a');
        if (n % 2 == 0) characters[0] = 'b';
        return new String(characters);
    }

    public static void main(String[] args) {
        System.out.println(generateTheString(8));
    }
}
