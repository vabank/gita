package LeetCode.com.five;

import java.util.Arrays;

//1299
//Replace Elements with Greatest Element on Right Side

public class ReplaceElements {

    public static int[] replaceElements(int[] arr) {
        int [] result = new int[arr.length];
        int max;
        for (int i = 1; i < arr.length; i++) {
            max = arr[i];
            for (int j = i+1; j < arr.length; j++) {
                max = Math.max(max,arr[j]);
            }
            result[i-1] = max;
        }
        result[arr.length-1] = -1;
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(replaceElements(new int[]{17, 18, 5, 4, 6, 1})));
    }

}
