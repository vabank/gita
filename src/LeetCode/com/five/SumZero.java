package LeetCode.com.five;

import java.util.Arrays;

//1304
//Find N Unique Integers Sum up to Zero

public class SumZero {

    public static int[] sumZero(int n) {
        int[] res = new int[n];
        int i = 0;

        if (n % 2 == 1) i++;

        for (int j = 1; j <= n/2; ++j) {
            res[i++] = j;
            res[i++] = -j;
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(sumZero(1)));
    }
}
