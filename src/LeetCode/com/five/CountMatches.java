package LeetCode.com.five;

import java.util.List;

//1773. Count Items Matching a Rule

public class CountMatches {
    public static int countMatches(List<List<String>> items, String ruleKey, String ruleValue) {
        int n = items.size();
        int k = 0;
        int ans=0;
        if (ruleKey.equals("color")) k=1;
        else if (ruleKey.equals("name")) k=2;
        for (List cur : items){
            if (cur.get(k).equals(ruleValue))
                ans++;
        }
        return ans;
    }

    public static void main(String[] args) {

  }
}
