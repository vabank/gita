package LeetCode.com.five;

import java.util.Arrays;

//942
//DI String Match

public class DiStringMatch {
    public static int[] diStringMatch(String s) {

        int highest = s.length();
        int lowest = 0;

        int[] tr = new int[s.length()+1];

        if (s.charAt(0) == 'D') {
            tr[0] = highest--;
        } else {
            tr[0] = lowest++;
        }

        for (int i=1;i<tr.length-1;i++) {

            if (s.charAt(i) == 'I') {

                tr[i] = lowest++;

            } else if (s.charAt(i) == 'D') {

                tr[i] = highest--;
            }
        }

        tr[tr.length-1] = lowest;

        return tr;

    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(diStringMatch("IDDD")));
    }

}
