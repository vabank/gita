package LeetCode.com.five;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

//1436
//Destination City

public class DestCity {

    public static String destCity(List<List<String>> paths) {
        HashMap<String,String> h=new HashMap<>(paths.size());
        for(List i:paths){
            h.put(i.get(0).toString(),i.get(1).toString());
        }
        Set s=h.keySet();
        for(Object i:s){
            if(!s.contains(h.get(i)))
                return h.get(i);
        }
        return null;
    }

    public static void main(String[] args) {

    }
}
