package LeetCode.com.five;

import java.util.HashMap;

//961
//N-Repeated Element in Size 2N Array

public class RepeatedNTimes {

    public int repeatedNTimes(int[] A) {
        HashMap<Integer,Integer> a = new HashMap<Integer,Integer>();

        for(int i=0; i<A.length;i++)
        {
            if(!a.containsValue(A[i]))
            {

                a.put(i,A[i]);
            }


            else{
                return A[i];
            }
        }
        return 0;

    }

}
