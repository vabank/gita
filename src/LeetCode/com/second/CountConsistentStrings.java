package LeetCode.com.second;

//  You are given a string allowed consisting of
//  distinct characters and an array of strings words.
//  A string is consistent if all characters in the string appear in the string allowed.
//
//  Return the number of consistent strings in the array words.
//
//
//
//        Example 1:
//
//        Input: allowed = "ab", words = ["ad","bd","aaab","baa","badab"]
//        Output: 2
//        Explanation: Strings "aaab" and "baa" are consistent since they only contain characters 'a' and 'b'.

public class CountConsistentStrings {

    public static int countConsistentStrings(String allowed, String[] words) {
        boolean isContains=false;
        int result = 0;
        String myString = "";
        for (String word : words) {
            for (int j = 0; j < word.length(); j++) {
                myString = "" + word.charAt(j);
                isContains = allowed.contains(myString);
                if (!isContains) {
                    break;
                }
            }
            if (isContains) {
                result++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        String[] word = new String[]{"cc","acd","b","ba","bac","bad","ac","d"};
        System.out.println(countConsistentStrings("cad", word));
    }
}
