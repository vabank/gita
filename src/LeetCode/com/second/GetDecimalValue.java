package LeetCode.com.second;



class GetDecimalValue {


    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }
    public int getDecimalValue(ListNode head) {

        ListNode curr = head;

        int size = -1;
        while (curr != null) {
            size++;
            curr = curr.next;
        }

        curr = head;
        int res = 0;
        while (curr != null) {
            if (curr.val == 1) {
                res += (Math.pow(2, size));
            }

            size--;
            curr = curr.next;
        }

        return res;
    }
}