package LeetCode.com.second;

//1295
//Find Numbers with Even Number of Digits

public class FindNumbers {

    public static int findNumbers(int[] nums) {
        int  result=0;
        for (int i = 0; i < nums.length; i++) {
            int digitCount = 0;
            while (nums[i]>0){
                nums[i]=nums[i]/10;
                digitCount++;
            }
            if (digitCount % 2 == 0) {
                result++;
            }


        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{12,345,2,6,7896};
        System.out.println(findNumbers(nums));
    }

}
