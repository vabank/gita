package LeetCode.com.second;

//1732
//Find the Highest Altitude

public class LargestAltitude {
    public int largestAltitude(int[] gain) {

        int max = Integer.MIN_VALUE, sum = 0;
        int[] myArray=new int[gain.length+1];
        myArray[0]=0;
        myArray[1]=gain[0];
        for (int i = 0; i < gain.length; i++) {
            sum=sum+gain[i];
            myArray[i + 1] = sum;
        }
        for (int j : myArray) {
            if (max < j) {
                max = j;
            }
        }

        return max;
    }
}
