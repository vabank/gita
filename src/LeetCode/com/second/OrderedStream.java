package LeetCode.com.second;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//1656
//Design an Ordered Stream

public class OrderedStream {

    private String[] values;
    private int ptr;

    public OrderedStream(int n) {
        values = new String[n];
        ptr = 0;
    }

    public List<String> insert(int id, String value) {
        values[id - 1] = value;

        List<String> result = new ArrayList();
        while (ptr < values.length && values[ptr] != null) {
            result.add(values[ptr++]);
        }

        return result;
    }

    public static void main(String[] args) {
        OrderedStream os = new OrderedStream(5);
        os.insert(3, "ccccc"); // Inserts (3, "ccccc"), returns [].
        os.insert(1, "aaaaa"); // Inserts (1, "aaaaa"), returns ["aaaaa"].
        os.insert(2, "bbbbb"); // Inserts (2, "bbbbb"), returns ["bbbbb", "ccccc"].
        os.insert(5, "eeeee"); // Inserts (5, "eeeee"), returns [].
        os.insert(4, "ddddd"); // Inserts (4, "ddddd"), returns ["ddddd", "eeeee"].
        System.out.println(Arrays.toString(os.values));
    }
}
