package LeetCode.com.second;

//	344
//Reverse String

public class ReverseString {

    public static String reverseString(char[] s) {
        String result = "";
        char[] myArray = new char[s.length];
        int start = 0, end = s.length-1;
        char tmp;
        while (start<=end){
            tmp = s[start];
            s[start] = s[end];
            s[end] = tmp;
            start++;
            end--;

        }
        return new String(s);
    }

    public static void main(String[] args) {
        char[] myArray = new char[]{'h', 'e', 'l', 'l', 'o'};
        System.out.println(reverseString(myArray));
    }

}
