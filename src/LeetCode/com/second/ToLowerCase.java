package LeetCode.com.second;

//	709
//To Lower Case

public class ToLowerCase {

    public static String toLowerCase(String str) {
        String result = "";
        char b;
        for (int i = 0; i < str.length(); i++) {
            int a=str.charAt(i);
            if (a > 96&&a<122) {
                b = (char) a;
                result = result + b;
            } else if (a>64&&a<90) {
                b = (char) (a + 32);
                result = result + b;
            }else{
                result=result+str.charAt(i);
            }

        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(toLowerCase("HeeeEDlDlo"));
    }

}
