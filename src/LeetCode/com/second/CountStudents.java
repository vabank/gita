package LeetCode.com.second;

//1700. Number of Students Unable to Eat Lunch

public class CountStudents {

    public static int countStudents(int[] students, int[] sandwiches) {
        int[] countZeroAndOne = new int[2];
        for (int i: students) countZeroAndOne[i]++;
        for (int i=0;i<sandwiches.length;i++)
            if (--countZeroAndOne[sandwiches[i]] < 0) return sandwiches.length - i;
        return 0;
    }

    public static void main(String[] args) {
        int[] students=new int[]{1,1,1,0,0,1};
        int[] sandwiches = new int[]{1,0,0,0,1,1};
        System.out.println(countStudents(students, sandwiches));
    }

}
