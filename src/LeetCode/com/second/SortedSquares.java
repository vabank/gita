package LeetCode.com.second;

import java.util.Arrays;

//977
//Squares of a Sorted Array

public class SortedSquares {

    public static int[] sortedSquares(int[] myArray) {
        int[] result = new int[myArray.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = myArray[i] * myArray[i];
        }

        for (int i = 0; i < result.length; i++) {
            for (int j = i + 1; j < result.length; j++) {
                int tmp = 0;
                if (result[i] > result[j]) {
                    tmp = result[i];
                    result[i] = result[j];
                    result[j] = tmp;
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] array = new int[]{-4, -1, 0, 3, 10};
        System.out.println(Arrays.toString(sortedSquares(array)));
    }
}
