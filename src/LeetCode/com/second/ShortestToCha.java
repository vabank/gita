package LeetCode.com.second;

import java.util.Arrays;

//821
//Shortest Distance to a Character

public class ShortestToCha {
    public static int[] shortestToChar(String S, char C) {

        int[] result = new int[S.length()];
        Arrays.fill(result, Integer.MAX_VALUE);

        for (int i = 0; i < S.length(); i++) {

            if (S.charAt(i) == C) {

                result[i] = 0;
                int diff = 1, left = i - 1, right = i + 1;

                while (left >= 0) {
                    result[left] = Math.min(result[left], diff);
                    left--;
                    diff++;
                }

                diff = 1;
                while (right < result.length) {
                    result[right] = Math.min(result[right], diff);
                    diff++;
                    right++;
                }
                for (int k : result) {
                    System.out.print(k + " ");
                }
                System.out.println();
            }
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(shortestToChar("loveleetcode", 'e')));
    }
}
