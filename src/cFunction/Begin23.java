package cFunction;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 21.01.2021
// Maqsad: N ning raqamlar sonini topish

public class Begin23 {

    static int digitCount(int n) {
        int count = 0;
        while (n != 0) {
            n = n / 10;
            count++;
        }
        return count;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Son kirit ukam: ");
        int n = scanner.nextInt();
        System.out.println(digitCount(n));


    }
}
