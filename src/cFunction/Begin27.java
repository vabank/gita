package cFunction;
// Muallif: Aziz Ismoilov
// Sana: 21.01.2021
// Maqsad: EKUKni topish

import java.util.Scanner;

public class Begin27 {

    private static int EKUB(int number1, int number2) {

            if(number2 == 0){
                return number1;
            }
            return EKUB(number2, number1%number2);
    }

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        System.out.println("number1 = ");
        int number1 = scanner.nextInt();
        System.out.println("number1 = ");
        int number2 = scanner.nextInt();
        System.out.println("EKUK " + number1 +" va "
                + number2 +" sonlari uchun : " + number1*number2/EKUB(number1,number2));
    }
}
