package cFunction;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 21.01.2021
// Maqsad: Kiritilgan yilning lyuboy oyidagi kunlar sonini topish

public class Begin31 {

    public static int isLeapYear(int year) {
            if (year % 4 != 0) {
                return 365;
            } else if (year % 400 == 0) {
                return 366;
            } else if (year % 100 == 0) {
                return 365;
            } else {
                return 366;
            }
        }

    public static int getDayCount(int leapCount, int month) {
        switch (month){
            case 1:
            case 10:
            case 8:
            case 7:
            case 5:
            case 12:
            case 3:
                return 31;
            case 2: return (leapCount==366)?29:28;
            case 4:
            case 11:
            case 9:
            case 6:
                return 30;
        }
        return 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0, month, year, leapCount;
        String monthDays = "";
        System.out.println("Yilni kiriting: ");
        year = scanner.nextInt();
        leapCount=isLeapYear(year);
        for (int i = 0; i < 3; i++) {
            System.out.println(i+1+" chi oyni kiriting: ");
            month = scanner.nextInt();

            monthDays = monthDays+getDayCount(leapCount, month)+" ";
        }
        System.out.println(monthDays);
    }
}
