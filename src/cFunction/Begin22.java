package cFunction;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 21.01.2021
// Maqsad: Kiritilgan son tub bo'lsa true yoki false ligini aniqlash

 public class Begin22 {

    public static boolean isPrimes(int n) {
        int num =0;
        int counter = 0;

        for(num =n; num>=1; num--) {

            if(n%num==0)
            {
                counter = counter + 1;
            }
        }
        return counter == 2;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta son kiritmoqchisiz?:");
        int k = scanner.nextInt();
        int[] myArray = new int[k];
        for (int i = 0; i <k; i++) {
            System.out.println(i+1+" chi elementni kiriting");
            myArray[i] = scanner.nextInt();
        }
        int count = 0;
            for (int j = 0; j < k; j++) {
                if (isPrimes(myArray[j])) {
                    count++;
                }
            }

            System.out.println("Nechtasi tub: " + count);
    }
}
