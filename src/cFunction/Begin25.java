package cFunction;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 21.01.2021
// Maqsad: isPalindrome func yaratish

public class Begin25 {

    static int digitN(int n) {

        int r,sum=0,reversedNumber;

        reversedNumber=n;
        while(n>0){
            r=n%10;
            sum=(sum*10)+r;
            n=n/10;
        }
        if (sum == reversedNumber) {
            return 1;
        }
        return 0;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int count = 0;
        for (int i = 0; i < 3; i++) {
            System.out.print(i+1+" chi sonni kiriting: ");
            int n = scanner.nextInt();
            count=count+digitN(n);
        }
        System.out.println(count+" tasi palidrom ekan oka");

    }
}
