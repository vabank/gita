package cFunction;
// Muallif: Aziz Ismoilov
// Sana: 21.01.2021
// Maqsad: 3 ta sonni EKUBni topish

import java.util.Scanner;

public class Begin29 {

    private static int EKUB(int number1, int number2) {
            if(number2 == 0){
                return number1;
            }
            return EKUB(number2, number1%number2);
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Nechta sonni EKUBini topmoqchisiz = ");
        int n = scanner.nextInt(),a = 0,b = 0,EKUB = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Son kiriting: ");
            if (i == 0) {
                a = scanner.nextInt();
            } else if (i == 1) {
                b = scanner.nextInt();
            }
            EKUB = EKUB(a, b);
            if(i>1) {
                b = scanner.nextInt();
                EKUB = EKUB(EKUB, b);
            }

        }
        System.out.println(EKUB);
    }
}
