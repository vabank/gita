package cFunction;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 21.01.2021
// Maqsad: Kiritilgan sanadan keyingi sanani aniqlovchi dastur tuzish

public class Begin33 {

    public static int isLeapYear(int year) {
            if (year % 4 != 0) {
                return 365;
            } else if (year % 400 == 0) {
                return 366;
            } else if (year % 100 == 0) {
                return 365;
            } else {
                return 366;
            }
        }

    public static int getDayCount(int leapCount, int month) {
        switch (month){
            case 1:
            case 10:
            case 8:
            case 7:
            case 5:
            case 12:
            case 3:
                return 31;
            case 2: return (leapCount==366)?29:28;
            case 4:
            case 11:
            case 9:
            case 6:
                return 30;
        }
        return 0;
    }

    public static void nextDate(int year, int month, int day) {
        int monthTotalDay, leapCount;
        leapCount=isLeapYear(year);
        if (leapCount == 365 && day != 29) {
            if (getDayCount(leapCount, month) == day) {
                if (month == 12) {
                    System.out.println(1 + "/" + 1 + "/" + (year + 1));
                } else {
                    System.out.println(1 + "/" + (month + 1) + "/" + year);
                }
            } else {
                System.out.println((day + 1) + "/" + month + "/" + year);
            }
        } else {
            System.out.println("Bunday sana yo'q");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int monthTotalDay, month, year, leapCount, day;
        System.out.println("Kunni kiriting: ");
        day = scanner.nextInt();
        System.out.println("oylarni kiriting: ");
        month = scanner.nextInt();
        System.out.println("Yilni kiriting: ");
        year = scanner.nextInt();
        nextDate(year, month, day);
    }
}
