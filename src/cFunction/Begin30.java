package cFunction;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 21.01.2021
// Maqsad: Kiritilgan yil kabisaligi yoki ligimasligini aniqlash

public class Begin30 {

    public static int isLeapYear(int year) {

            if (year % 4 != 0) {
                return 365;
            } else if (year % 400 == 0) {
                return 366;
            } else if (year % 100 == 0) {
                return 365;
            } else {
                return 366;
            }
        }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0, a;

        for (int i = 0; i < 3; i++) {
            System.out.println(i+1+" chi yilni kiriting: ");
            a = scanner.nextInt();
            if (isLeapYear(a)== 366) {
                count++;
            }
        }
        System.out.println(count);
    }
}
