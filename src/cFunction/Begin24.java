package cFunction;

import java.util.Scanner;

// Muallif: Aziz Ismoilov
// Sana: 21.01.2021
// Maqsad: Sonning kiritilgan pozitsiyadagi raqamni topish

public class Begin24 {

    static int digitN(long n,long k) {
        int myN = (int) n, answer = 0, count = 0;

        while (n != 0) {
            n = n / 10;
            count++;
        }

        if (count < k) {
            return -1;
        } else {
            for (int i = 0; i <= count - k; i++) {
                answer= myN % 10;
                myN = myN / 10;
            }
            return answer;
        }
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Son kirit ukam: ");
        int n = scanner.nextInt();
        System.out.println("Kiritgan sonni nechanchi raqamini qaytarsin: ");
        int k = scanner.nextInt();
        System.out.println(digitN(n,k));

    }
}
