package gParam;

// Muallif: Aziz Ismoilov
// Sana: 26.01.2021
// Maqsad: Invert punksiyasini tuzish


import java.util.Scanner;

public class Begin66 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka massiv nechta elementdan tashkil topsin:  ");
        int number1 = scanner.nextInt();
        int[] a = new int[number1];
        for (int i = 0; i < number1; i++) {
            System.out.println(i+1+" chi elemntni kirg'azing");
            a[i] = scanner.nextInt();
        }
        int[] myResult = new int[number1];
        myResult = invert(a, number1);
        for (int i = 0; i < number1; i++) {
            System.out.print(myResult[i] + " ");
        }

    }

    static int[] invert(int[] arr, int arrLength) {
        int[] result = new int[arrLength];
        int a = arrLength-1;
        for (int i = 0; i < arrLength; i++) {
            result[a] = arr[i];
            a--;
        }
        return result;
        }

}
