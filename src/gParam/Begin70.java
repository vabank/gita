package gParam;

// Muallif: Aziz Ismoilov
// Sana: 27.01.2021
// Maqsad: mxn o'lchamli matritsaning siz tanlagan lyuboy
// satr elemntlarining jig'indisini chiqazish


import java.util.Scanner;

public class Begin70 {

    public static void sumRow(int m, int n, int k) {
        int x = 1;
        int sum = 0;
        int[][] result = new int[m][n];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                result[i - 1][j - 1] = x++;
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(result[i][j]+"   ");
            }
            System.out.println();
        };

        for (int i = k-1; i <k; i++) {
            for (int j = 0; j < n; j++) {

                sum = sum +result[k-1][j];
            }
            System.out.print("Javob: "+sum);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qatorlar sonini kiriting m = ");
        int m = scanner.nextInt();
        System.out.println("Ustunlar sonini kiriting n = ");
        int n = scanner.nextInt();
        System.out.println("Oka nechanchi satr elemntlarining jig'indisini chiqarek k = ");
        int k = scanner.nextInt();
        if (k > n) {
            System.out.println("0");
        } else {
            sumRow(m, n, k);

        }
    }

}
