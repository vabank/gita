package gParam;

// Muallif: Aziz Ismoilov
// Sana: 26.01.2021
// Maqsad: Param


import java.util.Scanner;

public class Begin65 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka massiv nechta elementdan tashkil topsin:  ");
        int number1 = scanner.nextInt();
        int[] a = new int[number1];
        for (int i = 0; i < number1; i++) {
            System.out.println(i+1+" chi elemntni kirg'azing");
            a[i] = scanner.nextInt();
        }
        System.out.println("Eng kichu element: "+minElement(a, number1));

    }

    static int minElement(int[] arr, int arrLength) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < arrLength; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        return min;
    }

}
