package gParam;

// Muallif: Aziz Ismoilov
// Sana: 26.01.2021
// Maqsad: doubleX punksiyasini tuzish


import java.util.Scanner;

public class Begin68 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka massiv nechta elementdan tashkil topsin:  ");
        int number1 = scanner.nextInt();
        int[] a = new int[number1];
        for (int i = 0; i < number1; i++) {
            System.out.println(i+1+" chi elemntni kirg'azing");
            a[i] = scanner.nextInt();
        }
        System.out.println("Nechiga teng elementi ikkilantirilsin:  ");
        int removeNumber = scanner.nextInt();
        int[] myResult= doubleX(a, number1, removeNumber);
        for (int j : myResult) {
            System.out.print(j + " ");
        }

    }

    static int[] doubleX(int[] arr, int arrLength, int x) {
        int counter = 0;
        for (int i = 0; i < arrLength; i++) {
            if (x == arr[i]) {
                counter++;
            }
        }
        int[] result = new int[arrLength + counter];
        counter = 0;
        for (int i = 0; i < arrLength; i++) {
            if (arr[i] == x) {
                result[counter] = arr[i];
                counter++;
                result[counter] = arr[i];
            } else {
                result[counter] = arr[i];

            }
            counter++;
        }

        return result;
        }

}
