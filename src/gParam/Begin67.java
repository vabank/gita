package gParam;

// Muallif: Aziz Ismoilov
// Sana: 26.01.2021
// Maqsad: RemoveX punksiyasini tuzish


import java.util.Scanner;

public class Begin67 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka massiv nechta elementdan tashkil topsin:  ");
        int number1 = scanner.nextInt();
        int[] a = new int[number1];
        for (int i = 0; i < number1; i++) {
            System.out.println(i+1+" chi elemntni kirg'azing");
            a[i] = scanner.nextInt();
        }
        System.out.println("Nechiga teng elementi o'chirilsin:  ");
        int removeNumber = scanner.nextInt();
        int[] myResult= removeX(a, number1, removeNumber);
        for (int i = 0; i < myResult.length; i++) {
            System.out.print(myResult[i] + " ");
        }

    }

    static int[] removeX(int[] arr, int arrLength, int x) {
        int counter = 0;
        for (int i = 0; i < arrLength; i++) {
            if (x == arr[i]) {
                counter++;
            }
        }
        int[] result = new int[arrLength - counter];
        counter = 0;
        for (int i = 0; i < arrLength; i++) {
            if (arr[i] != x) {
                result[counter] = arr[i];
                counter++;
            }
        }

        return result;
        }

}
