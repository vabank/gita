package gParam;

// Muallif: Aziz Ismoilov
// Sana: 26.01.2021
// Maqsad: Massiv elementlarining indeksini saqlovchi massiv sozdat qilishs


import java.util.Scanner;

public class Begin69 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Oka massiv nechta elementdan tashkil topsin:  ");
        int number1 = scanner.nextInt();
        int[] a = new int[number1];
        for (int i = 0; i < number1; i++) {
            System.out.println(i+1+" chi elemntni kirg'azing");
            a[i] = scanner.nextInt();
        }
        for (int i = 0; i < number1; i++) {
            System.out.print(a[i]+" ");
        }
        System.out.println();
        int[] index= sortIndex(a, number1);
        for (int j : index) {
            System.out.print(j + " ");
        }

    }


    static int[] sortIndex(int[] arr, int arrLength) {
        int[] copyArray = new int[arrLength];
        if (arrLength >= 0) System.arraycopy(arr, 0, copyArray, 0, arrLength);
        int[] index = new int[arrLength];
        for (int i = 1; i < arrLength; i++) {
            int current = arr[i];
            int j = i - 1;
            while(j >= 0 && current < arr[j]) {
                arr[j+1] = arr[j];
                j--;
            }
            arr[j+1] = current;
        }

        for (int i = 0; i < arrLength; i++) {
            for (int j = 0; j <arrLength; j++) {
                    if (copyArray[i] == arr[j]) {
                        index[i] = j;
                    }
               }
        }
        return index;
        }

}
